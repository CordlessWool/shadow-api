import jwtDecode from 'jwt-decode';

export const parseJwt = (token) => {
  return jwtDecode(token);
};
