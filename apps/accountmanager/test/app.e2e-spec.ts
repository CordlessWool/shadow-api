import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import * as faker from 'faker';
import { Role } from '@lib/enum';
import { AccountmanagerModule } from '../src/accountmanager.module';
import { parseJwt } from './utils';

const sleep = (time: number): Promise<void> => {
  return new Promise((res) => {
    setTimeout(res, time);
  });
};

describe('AuthService (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AccountmanagerModule],
    }).compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  describe('Create a new Account', () => {
    let server;

    beforeAll(async () => {
      server = await app.getHttpServer();
    });

    it('create an account', async () => {
      await request(server)
        .post('/accounts')
        .send({
          domain: faker.internet.url(),
          email: faker.internet.email(),
          name: faker.name.findName(),
          password: faker.internet.password(),
        })
        .expect(201);
    });
  });

  describe('Auth and renew jwt', () => {
    let server;
    let accountData;

    beforeAll(async () => {
      server = app.getHttpServer();
      accountData = {
        domain: faker.internet.domainName(),
        email: faker.internet.email(),
        name: faker.name.findName(),
        password: faker.internet.password(),
      };
      await request(server).post('/accounts').send(accountData);
    });

    it('auth with username and password', async () => {
      const res = await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
          password: accountData.password,
        })
        .expect(201);

      expect(res.text).toBeDefined();
      expect(typeof res.text).toBe('string');
    });

    it('do not accept wrong password', async () => {
      await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
          password: faker.internet.password(),
        })
        .expect(401);
    });

    it('do not accept login wiht empty string password', async () => {
      await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
          password: '',
        })
        .expect(401);
    });

    it('do not accept login wihtout password', async () => {
      await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
        })
        .expect(401);
    });

    it('simple hacking username', async () => {
      await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: '1 or 1=1',
          password: accountData.password,
        })
        .expect(401);
    });

    it('check content of jwt', async () => {
      const res = await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
          password: accountData.password,
        })
        .expect(201);

      const jwt = res.text;

      const data = parseJwt(jwt) as Record<string, unknown>;

      expect(data.email).toBe(accountData.email);
      expect(data.name).toBe(accountData.name);
      expect(data.role).toBe(Role.owner);
      expect(data.password).not.toBeDefined();
    });

    it('domain information is requiered', async () => {
      await request(server)
        .post('/auth')
        .send({
          username: accountData.email,
          password: accountData.password,
        })
        .expect(404);
    });

    it('renew jwt', async () => {
      const res = await request(server)
        .post('/auth')
        .set('Host', accountData.domain)
        .send({
          username: accountData.email,
          password: accountData.password,
        })
        .expect(201);

      const firstJwt = res.text;
      const firstData = parseJwt(firstJwt) as Record<string, unknown>;

      await sleep(1000);

      const newRes = await request(server)
        .get('/auth/renew')
        .set('Authorization', `Bearer ${firstJwt}`)
        .expect(200);

      const renewed = newRes.text;
      const renewedData = parseJwt(renewed) as Record<string, unknown>;

      expect(renewed).toBeDefined();
      expect(typeof renewed).toBe('string');
      expect(renewedData.iat).toBeGreaterThan(<number>firstData.iat);
      expect(renewedData.exp).toBeGreaterThan(<number>firstData.exp);
    });

    it('renew jwt need jwt', async () => {
      await request(server)
        .get('/auth/renew')
        .set('Authorization', `Bearer ${faker.datatype.hexaDecimal(352)}`)
        .expect(401);
    });

    it('do not accept expiered', async () => {
      await request(server)
        .get('/auth/renew')
        .set(
          'Authorization',
          `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoib3duZXIiLCJpZCI6ImQzZDc0NjYwLTc3ODMtNGY4OC05ZWY5LTAwN2EzMzQyNmE4ZCIsImFjY291bnQiOiIwMmJlZmZlZS02NTRiLTQxNDctODY0ZC1mYmIzMWYwMTE4MTIiLCJuYW1lIjoiTWFuZHkgQmVpZXIiLCJlbWFpbCI6IldpbGx5X1Jvb2I2NEBnbWFpbC5jb20iLCJpYXQiOjE2MzA1MzY5NTIsImV4cCI6MTYzMDUzNzM3Mn0.N03BV6Bc7PqZM7ZyikWvy8fmlK6aq_0P07ijbtRqc4c`,
        )
        .expect(401);
    });
  });
});
