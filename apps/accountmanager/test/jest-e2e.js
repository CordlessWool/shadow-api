const { pathsToModuleNameMapper } = require('ts-jest/utils');
const tsConfig = require('../../../tsconfig.json');

module.exports = {
  moduleDirectories: ['node_modules'],
  moduleNameMapper: pathsToModuleNameMapper(tsConfig.compilerOptions.paths, {
    prefix: '<rootDir>/../..',
  }),
  moduleFileExtensions: ['js', 'json', 'ts'],
  rootDir: '../',
  testEnvironment: 'node',
  testRegex: '.e2e-spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: '../../../coverage',
  coverageReporters: ['text', 'text-summary', 'cobertura'],
  collectCoverageFrom: ['<rootDir>/src/**/*.(t|j)s'],
  coveragePathIgnorePatterns: ['.spec.ts'],
};
