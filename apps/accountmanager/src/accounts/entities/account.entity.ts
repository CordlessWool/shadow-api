import {
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Domain } from '../../domains/entities/domain.entity';
import { User } from '../../users/entities/user.entity';
import { accountId } from '../types-and-interfaces/id.types';

@Entity()
export class Account {
  @PrimaryGeneratedColumn('uuid')
  id: accountId;

  @OneToMany(() => Domain, (domain: Domain) => domain.account, {
    cascade: ['remove'],
  })
  domains: Domain[];

  @OneToMany(() => User, (user: User) => user.account, { cascade: ['remove'] })
  users: User[];

  @OneToOne(() => User, (user: User) => user.account, { cascade: ['remove'] })
  @JoinColumn()
  owner: User;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
