import { IsEmail, IsString, IsUrl } from 'class-validator';

export class CreateAccountDto {
  @IsUrl()
  domain: string;

  @IsEmail()
  email: string;

  @IsString()
  name: string;

  @IsString()
  password: string;
}
