import { EntityRepository, Repository } from 'typeorm';
import { Account } from './entities/account.entity';

@EntityRepository(Account)
export class AccountsReposotory extends Repository<Account> {
  async hasAccount(): Promise<boolean> {
    const amount = await this.count();

    if (amount === 0) {
      return false;
    }

    return true;
  }
}
