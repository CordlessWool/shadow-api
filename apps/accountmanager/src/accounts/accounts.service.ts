import {
  Injectable,
  InternalServerErrorException,
  MethodNotAllowedException,
  NotFoundException,
} from '@nestjs/common';
import { AccountConfigService } from '../account-config/account-config.service';
import { DomainsService } from '../domains/domains.service';
import { RoleEnum } from '../users/entities/role.entity';
import { UsersService } from '../users/users.service';
import { AccountsReposotory } from './accounts.reposotory';
import { CreateAccountDto } from './dto/create-account.dto';
import { Account } from './entities/account.entity';
import { accountId } from './types-and-interfaces/id.types';

@Injectable()
export class AccountsService {
  singleInstance: boolean;

  constructor(
    private readonly accountsRepo: AccountsReposotory,
    private readonly usersService: UsersService,
    private readonly domainsService: DomainsService,
    private readonly configService: AccountConfigService,
  ) {
    this.singleInstance = this.configService.get('SINGLE_INSTANCE');
  }

  async create(createAccountDto: CreateAccountDto): Promise<void> {
    const hasAccount = await this.accountsRepo.hasAccount();

    if (this.singleInstance && hasAccount) {
      throw new MethodNotAllowedException();
    }

    let account;

    try {
      const preAccount = new Account();
      account = await this.accountsRepo.save(preAccount);

      await this.domainsService.create({
        accountId: account.id,
        url: createAccountDto.domain,
      });

      await this.usersService.create({
        ...createAccountDto,
        account: account.id,
        role: RoleEnum.owner,
      });
    } catch (err) {
      if (account != null) {
        this.hardRemove(account);
      }

      throw new InternalServerErrorException(err);
    }
  }

  findAll() {
    return `This action returns all accounts`;
  }

  findOne(id: number) {
    return `This action returns a #${id} account`;
  }

  async remove(id: accountId) {
    const account = await this.accountsRepo.findOne(id);
    if (account == null) {
      throw new NotFoundException();
    }
    this.removeByAccount(account);
  }

  private async removeByAccount(account: Account, soft = true) {
    if (soft) {
      await this.accountsRepo.softRemove(account);
    } else {
      await this.accountsRepo.remove(account);
    }
  }

  private async hardRemove(account: Account) {
    await this.accountsRepo.remove(account);
  }
}
