import { Test, TestingModule } from '@nestjs/testing';
import { AccountConfigService } from '../account-config/account-config.service';
import { DomainsService } from '../domains/domains.service';
import { UsersService } from '../users/users.service';
import { AccountsReposotory } from './accounts.reposotory';
import { AccountsService } from './accounts.service';

describe('AccountsService', () => {
  let service: AccountsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountsService,
        {
          provide: AccountsReposotory,
          useValue: {},
        },
        {
          provide: UsersService,
          useValue: {},
        },
        {
          provide: DomainsService,
          useValue: {},
        },
        {
          provide: AccountConfigService,
          useValue: {
            get: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AccountsService>(AccountsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
