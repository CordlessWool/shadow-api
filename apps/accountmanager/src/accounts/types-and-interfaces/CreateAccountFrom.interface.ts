import { domainId } from './id.types';

export interface CreateAccountFrom {
  domain: domainId;
}
