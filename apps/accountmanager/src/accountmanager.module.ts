import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { APP_GUARD } from '@nestjs/core';
import { AccountsModule } from './accounts/accounts.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { DomainsModule } from './domains/domains.module';
import { AccountConfigModule } from './account-config/account-config.module';
import { AccountConfigService } from './account-config/account-config.service';

@Module({
  imports: [
    AccountConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [AccountConfigService],
      useFactory: (configService: AccountConfigService) => {
        const type = configService.get('DATABASE__TYPE');

        let database = configService.get('DATABASE__NAME');
        let schema: string | undefined = configService.get('DATABASE__ACCOUNT');
        if (database == null) {
          database = schema;
          schema = undefined;
        }
        return {
          type,
          host: configService.get('DATABASE__HOST'),
          port: configService.get('DATABASE__PORT'),
          username: configService.get('DATABASE__USERNAME'),
          password: configService.get('DATABASE__PASSWORD'),
          database,
          schema,
          entities: [`${__dirname}/**/*.entity{.ts,.js}`],
          // logging: ['query']
          synchronize: true,
          autoLoadEntities: true,
        };
      },
      inject: [AccountConfigService],
    }),
    UsersModule,
    AccountsModule,
    AuthModule,
    DomainsModule,
    ConfigModule,
    AccountConfigModule,
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AccountmanagerModule {}
