import { transformAndValidate } from '@lib/utils/validation/transformAndValidate';
import { EnvironmentVariables } from './env.definition';

export const validate = transformAndValidate(EnvironmentVariables, {
  enableImplicitConversion: true,
});
