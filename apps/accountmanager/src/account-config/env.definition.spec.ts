import { plainToClass } from 'class-transformer';
import { EnvironmentVariables } from './env.definition';

const testBoolean = (env) => {
  const envs = {
    [env]: 'false',
  };

  do {
    const converted = plainToClass(EnvironmentVariables, envs);
    expect(converted[env]).toBe(envs[env] !== 'false');
    envs[env] = envs[env] === 'false' ? 'true' : 'false';
  } while (envs[env] === 'true');
};

describe('test env defintions file', () => {
  it('check if defaults are set', () => {
    const variables = new EnvironmentVariables();
    expect(variables.HOST).toBeDefined();
    expect(variables.PORT).toBeDefined();
    expect(variables.DATABASE__SSL).toBe(true);
  });

  it('check convertion of DATABASE__SSL', () => {
    testBoolean('DATABASE__SSL');
  });
});
