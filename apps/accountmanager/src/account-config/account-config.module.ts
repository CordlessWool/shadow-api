import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { validate } from './env.validation';
import { AccountConfigService } from './account-config.service';

@Module({
  providers: [AccountConfigService],
})
export class AccountConfigModule {
  static forRoot(): DynamicModule {
    return {
      module: AccountConfigModule,
      global: true,
      imports: [
        ConfigModule.forRoot({
          validate,
        }),
      ],
      providers: [AccountConfigService],
      exports: [AccountConfigService],
    };
  }
}
