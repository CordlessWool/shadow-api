/* eslint-disable @typescript-eslint/no-inferrable-types */
import { NodeEnv } from '@lib/enum';
import { Transform } from 'class-transformer';

import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export enum DatabaseTypeEnum {
  mysql = 'mysql',
  mariadb = 'mariadb',
  postgres = 'postgres',
}

export class EnvironmentVariables {
  @IsEnum(NodeEnv)
  NODE_ENV: NodeEnv;

  @IsString()
  HOST = 'localhost';

  @IsNumber()
  PORT = 3010;

  // @Type(() => Booolean) =>
  @Transform(({ value }) => Boolean(value.SINGLE_INSTANCE))
  @IsBoolean()
  SINGLE_INSTANCE: boolean = false;

  @IsEnum(DatabaseTypeEnum)
  DATABASE__TYPE: keyof typeof DatabaseTypeEnum;

  @IsString()
  DATABASE__HOST: string;

  @IsNumber()
  DATABASE__PORT: number;

  @Transform(({ obj }) => obj.DATABASE__SSL === 'true')
  @IsBoolean()
  DATABASE__SSL: boolean = true;

  @IsString()
  DATABASE__USERNAME: string;

  @IsString()
  DATABASE__PASSWORD: string;

  @IsOptional()
  @IsString()
  DATABASE__NAME: string;

  @IsString()
  DATABASE__ACCOUNT: string;

  @IsString()
  JWT__TTL: string | number = '7m';

  @IsString()
  @IsOptional()
  JWT__KEY?: string;

  @IsString()
  @IsOptional()
  JWT__PUBLIC?: string;

  @IsString()
  @IsOptional()
  JWT__SECRET?: string;
}
