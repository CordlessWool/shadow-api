import { JwtUserData } from '@lib/interfaces';
import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { DomainsService } from '../domains/domains.service';
import { TrimmedUser } from '../users/types-and-interfaces/trimmer-user.type';
import { UsersService } from '../users/users.service';
import { Crypter } from '../utils/Crypter';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly domainsService: DomainsService,
    private readonly jwtService: JwtService,
  ) {}

  async login(user: TrimmedUser): Promise<string> {
    const { id, accountId, role, name, email } = user;
    const payload: JwtUserData = {
      role,
      userId: id,
      accountId,
      name,
      email,
    };
    return this.jwtService.signAsync(payload);
  }

  async validateUser(
    hostname: string,
    email: string,
    password: string,
  ): Promise<TrimmedUser> {
    const domain = await this.domainsService.findOne(hostname);
    if (!domain) {
      throw new NotFoundException();
    }
    const { password: encrypted, ...user } =
      (await this.usersService.findOne(email, domain.accountId)) || {};
    if (!encrypted) {
      throw new UnauthorizedException();
    }
    const isValid: boolean = await Crypter.comparePassword(password, encrypted);
    if (isValid) {
      return user;
    }

    throw new UnauthorizedException();
  }
}
