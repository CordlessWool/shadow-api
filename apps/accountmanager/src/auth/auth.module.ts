import { RequireExeption } from '@lib/exeptions/RequiredExeption';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AccountConfigService } from '../account-config/account-config.service';
import { DomainsModule } from '../domains/domains.module';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './stratigies/jwt.strategy';
import { LocalStrategy } from './stratigies/local.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (config: AccountConfigService) => {
        const ttl = config.get('JWT__TTL') || '7m';
        let secretOrKey = config.get('JWT__KEY');
        if (!secretOrKey) {
          secretOrKey = config.get('JWT__SECRET');
        }

        if (!secretOrKey) {
          throw new RequireExeption('secret or key');
        }
        return {
          secretOrPrivateKey: secretOrKey, // TODO: replace with async cert
          signOptions: { expiresIn: ttl },
        };
      },
      inject: [AccountConfigService],
    }),
    UsersModule,
    DomainsModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
})
export class AuthModule {}
