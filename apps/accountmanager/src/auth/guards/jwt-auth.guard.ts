import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Reflector } from '@nestjs/core';
import { Role } from '@lib/enum';
import { ROLES_KEY } from '../../static';
import { User } from '../../users/entities/user.entity';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') implements CanActivate {
  constructor(private readonly reflector: Reflector) {
    super();
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (requiredRoles?.includes(Role.public)) {
      return true;
    }

    const isValid = await super.canActivate(context);

    if (!isValid) return false;

    const { user } = context.switchToHttp().getRequest<{ user: User }>();

    if (user?.role === Role.admin || user?.role === Role.owner) {
      return true;
    }

    return requiredRoles?.some((role) => user.role === role);
  }
}
