import { Role } from '@lib/enum';
import { SetMetadata } from '@nestjs/common';
import { ROLES_KEY } from '../../static';

export const Roles = (...roles: Role[]) => SetMetadata(ROLES_KEY, roles);
export const Public = () => Roles(Role.public);
