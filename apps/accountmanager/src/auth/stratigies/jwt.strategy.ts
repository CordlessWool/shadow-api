import { RequireExeption } from '@lib/exeptions/RequiredExeption';
import { JwtUserData } from '@lib/interfaces';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { AccountConfigService } from '../../account-config/account-config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(config: AccountConfigService) {
    let secretOrKey = config.get('JWT__PUBLIC');
    if (!secretOrKey) {
      secretOrKey = config.get('JWT__SECRET');
    }

    if (!secretOrKey) {
      throw new RequireExeption('secret or key');
    }
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey,
    });
  }

  async validate(payload: JwtUserData) {
    return payload;
  }
}
