import { Role } from '@lib/enum';
import { NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { isJWT } from 'class-validator';
import * as faker from 'faker';
import { DomainsService } from '../domains/domains.service';
import { Domain } from '../domains/entities/domain.entity';
import { User } from '../users/entities/user.entity';
import { TrimmedUser } from '../users/types-and-interfaces/trimmer-user.type';
import { UsersService } from '../users/users.service';
import { Crypter } from '../utils/Crypter';
import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let jwtService: JwtService;
  let password: string;
  const userName = 'valid user';

  const userServiceMock = {
    findOne: jest.fn(async () => {
      const user = new User();
      user.name = userName;
      user.password = await Crypter.encryptPassword(password);
      return Promise.resolve(user);
    }),
  };

  const domainsServiceMock = {
    findOne: jest.fn((host): ReturnType<DomainsService['findOne']> => {
      const domain = new Domain();
      domain.url = host;
      domain.accountId = faker.datatype.uuid();
      return Promise.resolve(domain);
    }),
  };

  beforeEach(async () => {
    password = faker.internet.password();
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        JwtModule.register({
          secret: 'someSecret',
          signOptions: { expiresIn: '7' },
        }),
      ],
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: userServiceMock,
        },
        {
          provide: DomainsService,
          useValue: domainsServiceMock,
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validate User', () => {
    it('valid user', async () => {
      const user = await service.validateUser(
        'localhost',
        'some@shadow.photo',
        password,
      );

      expect(user.name).toBe(userName);
      expect((<User>user).password).toBeUndefined();
    });

    it('wrong password', async () => {
      try {
        const user = await service.validateUser(
          'localhost',
          'some@shadow.photo',
          faker.internet.password(),
        );
        expect(user).toBeUndefined();
      } catch (err) {
        expect(err).toBeInstanceOf(UnauthorizedException);
      }
    });

    it('not existing domain', async () => {
      domainsServiceMock.findOne.mockReturnValueOnce(
        Promise.resolve(undefined),
      );
      try {
        await service.validateUser('localhost', 'some@shadow.photo', password);
        expect('user').toBeUndefined();
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
      }
    });
  });

  describe('login', () => {
    let user: TrimmedUser;

    beforeEach(() => {
      user = {
        id: faker.datatype.uuid(),
        role: Role.admin,
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
      };
    });

    it('get jwt', async () => {
      const token = await service.login(user);
      expect(typeof token).toBe('string');
      expect(isJWT(token)).toBe(true);
    });

    it('jwt contains data', async () => {
      const token = await service.login(user);
      const data = <Record<string, unknown>>(jwtService.decode(token) || {});

      const listOfRequirements = [
        'role',
        'userId',
        'accountId',
        'name',
        'email',
      ];

      for (const i of listOfRequirements) {
        expect(Object.keys(data)).toContain(i);
      }
    });

    it('password is not part of the jwt', async () => {
      const userWithPassword = {
        ...user,
        password,
      };

      const token = await service.login(userWithPassword);
      const data = <Record<string, unknown>>(jwtService.decode(token) || {});

      Object.keys(data).forEach((value, i) => {
        expect(i).not.toBe('password');
        expect(value).not.toBe(password);
      });
    });

    it('set roles: author', async () => {
      user.role = Role.author;

      const token = await service.login(user);
      const data = <Record<string, unknown>>(jwtService.decode(token) || {});
      expect(data.role).toBe(Role.author);
    });

    it('set roles: photographer', async () => {
      user.role = Role.photographer;

      const token = await service.login(user);
      const data = <Record<string, unknown>>(jwtService.decode(token) || {});
      expect(data.role).toBe(Role.photographer);
    });

    it('set roles: not-auth', async () => {
      user.role = Role.public;

      const token = await service.login(user);
      const data = <Record<string, unknown>>(jwtService.decode(token) || {});
      expect(data.role).toBe(Role.public);
    });
  });
});
