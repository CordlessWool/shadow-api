import { IsUrl, IsUUID } from 'class-validator';

export class CreateDomainDto {
  @IsUrl()
  url: string;

  @IsUUID()
  accountId: string;
}
