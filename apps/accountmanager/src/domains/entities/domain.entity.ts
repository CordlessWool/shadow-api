import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryColumn,
  RelationId,
  UpdateDateColumn,
} from 'typeorm';
import { Account } from '../../accounts/entities/account.entity';
import { accountId } from '../../accounts/types-and-interfaces/id.types';

@Entity()
export class Domain {
  @PrimaryColumn()
  url: string;

  @Column({
    default: true,
  })
  valid: boolean;

  @Column({
    default: new Date(),
  })
  lastValidation: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => Account, (account: Account) => account.domains)
  account: Account;

  @RelationId((domain: Domain) => domain.account)
  @Column()
  accountId: accountId;
}
