import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { accountId } from '../accounts/types-and-interfaces/id.types';
import { CreateDomainDto } from './dto/create-domain.dto';
import { Domain } from './entities/domain.entity';

@Injectable()
export class DomainsService {
  constructor(
    @InjectRepository(Domain) private readonly domainRepo: Repository<Domain>,
  ) {}

  async create(dto: CreateDomainDto): Promise<Domain> {
    const domain = new Domain();
    domain.url = dto.url;
    domain.accountId = dto.accountId;

    return this.domainRepo.save(domain);
  }

  findAll() {
    return `This action returns all domains`;
  }

  findOne(url: string): Promise<Domain | undefined> {
    return this.domainRepo.findOne(url);
  }

  remove(id: number) {
    return `This action removes a #${id} domain`;
  }

  async removeByAccountId(id: accountId, soft = true) {
    if (soft === true) {
      this.domainRepo.softDelete({
        accountId: id,
      });
    } else {
      this.domainRepo.delete({ accountId: id });
    }
  }
}
