/**
 * Created by wolle on 7/11/16.
 */

import { pbkdf2, randomBytes } from 'crypto';

export class Crypter {
  static async getSalt(): Promise<string> {
    return new Promise((resolve, reject) => {
      randomBytes(512, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const salt = buf.toString('base64');
        return resolve(salt);
      });
    });
  }

  static async encrypt(
    password: string,
    salt: string,
    iteration = 10000,
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      pbkdf2(password, salt, iteration, 512, 'sha512', (err, key) => {
        if (err) return reject(err);
        const skey: string = key.toString('base64');
        return resolve(skey);
      });
    });
  }

  static async encryptPassword(password: string): Promise<string> {
    const iteration = 10000;
    const salt = await Crypter.getSalt();

    const key = await Crypter.encrypt(password, salt, iteration);

    return `${salt}::${key}::${iteration}`;
  }

  static async comparePassword(
    password: string,
    encrypted: string,
  ): Promise<boolean> {
    const ingredients = encrypted.split('::');
    if (ingredients.length !== 3) return false;
    const key = await Crypter.encrypt(
      password,
      ingredients[0],
      parseInt(ingredients[2], 10),
    );
    if (ingredients[1] === key) {
      return true;
    }

    return false;
  }
}
