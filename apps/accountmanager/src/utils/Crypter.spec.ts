import * as faker from 'faker';
import { Crypter } from './Crypter';

describe('Crypter for Passwords', () => {
  it('create Hash with salt and iterations', async () => {
    const password = faker.internet.password();
    const encrypted = await Crypter.encryptPassword(password);
    const ingredients = encrypted.split('::');
    expect(ingredients.length).toBe(3);
    expect(ingredients[0]).not.toBe(password);
    expect(ingredients[1].length).toBeGreaterThanOrEqual(512);
    expect(parseInt(ingredients[2], 10)).toBeGreaterThanOrEqual(10000);
  });

  it('test long passwords', async () => {
    const length = 512;
    const password = faker.internet.password(length);
    expect(password.length).toBe(length);
    const encrypted = await Crypter.encryptPassword(password);
    expect(encrypted.length).toBeGreaterThan(length);
    const ingredients = encrypted.split('::');
    expect(ingredients[0].length).toBeGreaterThan(length);
  });

  describe('compare passwords', () => {
    it('long password with a short one', async () => {
      const length = 1024;
      const password = faker.internet.password(length);
      const comparePassword = password.slice(0, length - 10);
      expect(password.length).toBe(length);
      const encrypted = await Crypter.encryptPassword(password);
      expect(await Crypter.comparePassword(comparePassword, encrypted)).toBe(
        false,
      );
    });

    it('passwords with different signeds', async () => {
      const password = 'kljasdfi123154?)/(!)"§(!%&("§)!("§=)!")?';
      const encrypted = await Crypter.encryptPassword(password);
      expect(await Crypter.comparePassword(password, encrypted)).toBe(true);
    });

    it('compare password', async () => {
      const password = faker.internet.password();
      const encrypted = await Crypter.encryptPassword(password);
      expect(await Crypter.comparePassword(password, encrypted)).toBe(true);
    });

    it('compare differen passwords', async () => {
      const password = faker.internet.password();
      const other = faker.internet.password();
      expect(password).not.toBe(other);
      const encrypted = await Crypter.encryptPassword(password);
      expect(await Crypter.comparePassword(other, encrypted)).toBe(false);
    });
  });
});
