import { Role } from '@lib/enum';
import {
  Column,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { Account } from '../../accounts/entities/account.entity';
import { accountId } from '../../accounts/types-and-interfaces/id.types';
import { userId } from '../types-and-interfaces/id';
import { CreateUserInterface } from '../types-and-interfaces/user-data.interface';

@Entity()
@Index(['email', 'accountId'], { unique: true })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: userId;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column({
    length: 2048,
  })
  password: string;

  // @ManyToOne(() => Role, (role: Role) => role.users)
  // role: Role;

  // @RelationId((user: User) => user.role)
  @Column({
    enum: Role,
    type: 'varchar',
    length: 15,
    nullable: false,
  })
  role: Role;

  @ManyToOne(() => Account, (account: Account) => account.users)
  account?: Account;

  @RelationId((user: User) => user.account)
  @Column({
    nullable: false,
  })
  accountId: accountId;

  static from(data: CreateUserInterface): User {
    const user = new User();
    user.name = data.name;
    user.email = data.email;
    if (data.password) user.password = data.password;
    user.role = data.role;
    user.accountId = data.account;
    return user;
  }
}
