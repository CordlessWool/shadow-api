import { Column, Entity, PrimaryColumn } from 'typeorm';
import * as tpye from '@lib/enum';
import { roleId } from '../types-and-interfaces/id';

export const RoleEnum = tpye.Role;
@Entity()
export class Role {
  @PrimaryColumn({
    type: 'varchar',
    length: 10,
  })
  name: roleId;

  // @OneToMany(() => User, (user: User) => user.role)
  // users: User[];

  @Column()
  description: string;
}
