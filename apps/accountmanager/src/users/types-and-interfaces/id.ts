import { Role } from '@lib/enum';

export type userId = string;

export type roleId = keyof typeof Role;
