import { Role } from '@lib/enum';

export interface CreateUserInterface {
  name: string;
  email: string;
  role: Role;
  account: string;
  password?: string;
}
