import { User } from '../entities/user.entity';

export type TrimmedUser = Omit<User, 'password'>;
