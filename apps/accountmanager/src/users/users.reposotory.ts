import { EntityRepository, Repository } from 'typeorm';
import { accountId } from '../accounts/types-and-interfaces/id.types';
import { User } from './entities/user.entity';

@EntityRepository(User)
export class UsersReposotory extends Repository<User> {
  async getPassword(email: string, account: accountId): Promise<string> {
    const users = await this.find({
      select: ['password'],
      where: {
        email,
        account,
      },
    });

    return users[0].password;
  }

  async findUser(email: string, account: accountId) {
    const users = await this.find({
      where: {
        email,
        accountId: account,
      },
    });

    return users[0];
  }
}
