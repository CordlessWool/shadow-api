import { Role } from '@lib/enum';
import { Test, TestingModule } from '@nestjs/testing';
import * as faker from 'faker';
import { Crypter } from '../utils/Crypter';
import { User } from './entities/user.entity';
import { CreateUserInterface } from './types-and-interfaces/user-data.interface';
import { UsersReposotory } from './users.reposotory';
import { UsersService } from './users.service';

const mockReposotory = {
  save: jest.fn(),
  find: jest.fn(),
  findAndCount: jest.fn(),
  findOne: jest.fn(),
  update: jest.fn(),
  removeById: jest.fn(),
  remove: jest.fn(),
};

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: UsersReposotory,
          useValue: mockReposotory,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create new User', () => {
    let spy: jest.SpyInstance;
    let saveValue: Partial<User>;
    beforeEach(() => {
      saveValue = {};
      spy = jest.spyOn(mockReposotory, 'save').mockImplementation((v: User) => {
        saveValue = v;
      });
    });
    it('encrypt passwords', async () => {
      const password = faker.internet.password();
      const data: CreateUserInterface = {
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.admin,
        account: faker.datatype.uuid(),
        password,
      };
      await service.create(data);
      const savedPass = saveValue.password;
      if (savedPass == null) {
        expect(savedPass).toBeDefined();
        return;
      }

      expect(savedPass).not.toBe(password);
      expect(savedPass.length).toBeGreaterThan(password.length);
      expect(Crypter.comparePassword(password, savedPass));
      expect(spy).toBeCalled();
    });
  });
});
