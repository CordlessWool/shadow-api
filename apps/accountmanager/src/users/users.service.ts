import { Injectable } from '@nestjs/common';
import { Crypter } from '../utils/Crypter';
import { User } from './entities/user.entity';
import { CreateUserInterface } from './types-and-interfaces/user-data.interface';
import { UsersReposotory } from './users.reposotory';

@Injectable()
export class UsersService {
  constructor(private readonly usersRepo: UsersReposotory) {}

  async create(createUserDto: CreateUserInterface) {
    const { password, ...data } = createUserDto;
    const userData: CreateUserInterface = data;
    if (password) {
      userData.password = await Crypter.encryptPassword(password);
    }

    const user = User.from(userData);
    return this.usersRepo.save(user);
  }

  findAll() {
    return `This action returns all users`;
  }

  async findOne(email: string, account: string) {
    return this.usersRepo.findUser(email, account);
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
