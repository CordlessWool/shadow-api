import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AccountConfigService } from './account-config/account-config.service';
import { AccountmanagerModule } from './accountmanager.module';

async function bootstrap() {
  const app = await NestFactory.create(AccountmanagerModule);

  const configService = app.get(AccountConfigService);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  await app.listen(configService.get('PORT'));
}
bootstrap();
