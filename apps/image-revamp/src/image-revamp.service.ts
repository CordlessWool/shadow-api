import { Injectable } from '@nestjs/common';
import * as sharp from 'sharp';
import { RevampNotNecassery } from './exeptions';
import { ImageRevamp } from './image-revamp.interface';

@Injectable()
export class ImageRevampService {
  private isSharpObject(image: unknown): image is sharp.Sharp {
    if (typeof (image as sharp.Sharp).metadata === 'function') {
      return true;
    }
    return false;
  }

  private async resize(image: sharp.Sharp, options: sharp.ResizeOptions) {
    const { fit, height: oHeight = 0, width: oWidth = 0 } = options;
    const { height: mHeight = 0, width: mWidth = 0 } = await image.metadata();

    if (fit === 'inside' && !(oHeight < mHeight || oWidth < mWidth)) {
      throw new RevampNotNecassery();
    } else if (fit === 'outside' && (oHeight >= mHeight || oWidth >= mWidth)) {
      throw new RevampNotNecassery();
    }

    image.rotate();
    image.resize(options);
  }

  async getMetadata(image: Buffer | sharp.Sharp) {
    let sharped: sharp.Sharp;
    if (this.isSharpObject(image)) {
      sharped = image;
    } else {
      sharped = sharp(image);
    }

    return sharped.metadata();
  }

  async transform(
    image: Buffer | string,
    options: ImageRevamp,
  ): Promise<Buffer> {
    const { format, blur } = options;

    const sharpImage = sharp(image);

    await this.resize(sharpImage, {
      fit: options.fit || 'outside',
      height: options.height,
      width: options.width,
    });
    if (format) sharpImage.toFormat(format);
    if (blur) sharpImage.blur();

    return sharpImage.toBuffer();
  }
}
