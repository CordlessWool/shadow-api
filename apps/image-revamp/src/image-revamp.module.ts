import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ImageRevampController } from './image-revamp.controller';
import { ImageRevampService } from './image-revamp.service';
import { FileService } from './file.service';

@Module({
  imports: [HttpModule],
  controllers: [ImageRevampController],
  providers: [ImageRevampService, FileService],
})
export class ImageRevampModule {}
