import { ExtendOptions, FitEnum, FormatEnum } from 'sharp';

export interface ImageRevamp {
  height?: number;
  width?: number;
  fit?: keyof FitEnum;
  format?: keyof FormatEnum;
  extend?: ExtendOptions;
  meta?: boolean;
  quality?: number;
  keepMeta?: boolean;
  blur?: boolean;
}
