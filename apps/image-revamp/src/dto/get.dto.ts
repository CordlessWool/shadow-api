import { IsUrl } from 'class-validator';
import { ImageRevampDto } from './image-revamp.dto';

export class GetDto extends ImageRevampDto {
  @IsUrl()
  src: string;
}
