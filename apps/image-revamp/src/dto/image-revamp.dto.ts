import { FitEnum, FormatEnum } from 'sharp';
import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  IsUrl,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ImageRevamp } from '../image-revamp.interface';

export class ImageRevampDto implements ImageRevamp {
  @Type(() => Number)
  @IsNumber()
  @IsPositive()
  @IsOptional()
  height?: number;

  @Type(() => Number)
  @IsNumber()
  @IsPositive()
  @IsOptional()
  width?: number;

  @IsString()
  @IsOptional()
  fit?: keyof FitEnum;

  @IsString()
  @IsOptional()
  format?: keyof FormatEnum;

  @Type(() => Number)
  @IsNumber()
  @IsPositive()
  @IsOptional()
  quality?: number;

  @Type(() => Boolean)
  @IsBoolean()
  @IsOptional()
  keepMeta?: boolean;

  @Type(() => Boolean)
  @IsBoolean()
  @IsOptional()
  blur?: boolean;
}

export class MultiRevampDto extends ImageRevampDto {
  @IsString()
  id: string;

  @IsUrl()
  url: string;
}
