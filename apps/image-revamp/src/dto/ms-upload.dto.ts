import { FormatFit, FormatType } from '@lib/enum';
import { ManipulationInterface } from '@lib/interfaces';
import { plainToClass } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import sharp from 'sharp';

import { IsConvertableToBuffer } from '../validators/isConvertableToBuffer';
import { MultiRevampDto } from './image-revamp.dto';

export class MSUploadDto {
  @IsConvertableToBuffer()
  image: string;

  @ValidateNested()
  formats: MultiRevampDto[];
}

export class MSUploadReturnDto implements ManipulationInterface {
  id: string;

  height: number;

  width: number;

  size: number;

  types: FormatType;

  fit: FormatFit;

  static fromMetadata(meta: sharp.Metadata, id?: string) {
    return plainToClass(MSUploadReturnDto, {
      id,
      ...meta,
    });
  }
}
