import { IsOptional, IsString } from 'class-validator';
import { ImageRevampDto } from './image-revamp.dto';
import { IsConvertableToBuffer } from '../validators/isConvertableToBuffer';

export class MSConvertDTO extends ImageRevampDto {
  @IsString()
  @IsOptional()
  id?: string;

  @IsConvertableToBuffer()
  image: string;
}
