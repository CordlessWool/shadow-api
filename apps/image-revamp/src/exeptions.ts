import { RpcException } from '@nestjs/microservices';

export class RevampNotNecassery extends RpcException {
  constructor() {
    super('Revamp not necassery');
    this.name = 'RevampNotNecassery';
  }
}
