import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ImageRevampModule } from './image-revamp.module';

async function bootstrap() {
  const port = 3010;

  const app = await NestFactory.create(ImageRevampModule);
  app.connectMicroservice<MicroserviceOptions>(
    {
      transport: Transport.NATS,
      options: {
        servers: ['nats://nats:4222'],
        queue: 'image_revamp',
      },
    },
    { inheritAppConfig: true },
  );
  await app.startAllMicroservices();
  await app.listen(port);
}
bootstrap();
