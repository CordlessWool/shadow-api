import { MSUploadReturnDto } from './dto/ms-upload.dto';
import { ImageRevamp } from './image-revamp.interface';

export type ImageJson = { type: 'Buffer'; data: number[] };
export type RevampReturn = { id?: string; image: ImageJson } & ImageRevamp;

export enum MessageStatus {
  warning = 'warning',
  info = 'info',
  error = 'error',
  ok = 'ok',
}

export enum MessageType {
  not_changed = 'not_changed',
  data = 'data',
  server = 'internal_server_error',
}

export interface Message<T = unknown, S = keyof typeof MessageStatus> {
  status: S;
  type: MessageType;
  data?: T;
  message?: string;
}

export type MSUploadReturnType =
  | Message<MSUploadReturnDto, MessageStatus.ok>
  | Message<
      Pick<MSUploadReturnDto, 'id'>,
      Exclude<keyof typeof MessageStatus, 'ok'>
    >;
