import {
  BadRequestException,
  Controller,
  Get,
  Query,
  Res,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { from, Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { Response } from 'express';
import { GetDto } from './dto/get.dto';
import { MSConvertDTO } from './dto/ms-convert.dto';
import { MSUploadDto, MSUploadReturnDto } from './dto/ms-upload.dto';
import { RevampNotNecassery } from './exeptions';
import { ImageRevampService } from './image-revamp.service';
import {
  Message,
  MessageStatus,
  MessageType,
  MSUploadReturnType,
} from './types';
import { FileService } from './file.service';

@Controller()
export class ImageRevampController {
  constructor(
    private readonly revampService: ImageRevampService,
    private readonly fileService: FileService,
  ) {}

  private catchMessage<T = never>(
    error: unknown,
    data?: T,
  ): Message<T, Exclude<keyof typeof MessageStatus, 'ok'>> {
    if (error instanceof RevampNotNecassery) {
      return {
        status: MessageStatus.info,
        type: MessageType.not_changed,
        message: error.message,
        data,
      };
    }
    console.error(error);
    return {
      status: MessageStatus.error,
      type: MessageType.server,
      data,
    };
  }

  @UsePipes(
    new ValidationPipe({
      transform: true,
    }),
  )
  @Get()
  async revamp(
    @Query() { src, ...options }: GetDto,
    @Res() response: Response,
  ): Promise<unknown> {
    console.info('request', src, options);
    let [image, type] = await this.fileService.getWithType(src);
    if (!type.startsWith('image')) {
      throw new BadRequestException('Content type is not supported');
    }
    if (Object.keys(options).length !== 0) {
      image = await this.revampService.transform(image, options);
      if (options.format) type = `image/${options.format}`;
    }
    return response
      .setHeader('content-type', type)
      .end(image.toString('base64'), 'base64');
  }

  @UsePipes(new ValidationPipe())
  @MessagePattern({ cmd: 'transform' })
  async tranformAndReturn({
    image,
    ...options
  }: MSConvertDTO): Promise<Message> {
    try {
      console.info('tranform', options);
      const transformed = await this.revampService.transform(
        Buffer.from(image, 'base64url'),
        options,
      );

      return {
        status: MessageStatus.ok,
        type: MessageType.data,
        data: transformed,
      };
    } catch (e) {
      return this.catchMessage(e);
    }
  }

  @UsePipes(new ValidationPipe())
  @MessagePattern({
    cmd: 'transform',
    upload: true,
    multi: true,
  })
  transformAndSave({
    image,
    formats,
  }: MSUploadDto): Observable<MSUploadReturnType> {
    // console.info('transform multi', formats);
    return from(formats).pipe(
      concatMap(async (format) => {
        try {
          const { id, url, ...revamp } = format;
          const revamped = await this.revampService.transform(
            Buffer.from(image, 'base64url'),
            revamp,
          );
          const meta = await this.revampService.getMetadata(revamped);
          await this.fileService.save(url, revamped, meta);
          // TODO: Optimice here to not transform it forward and back
          return {
            status: MessageStatus.ok,
            type: MessageType.data,
            data: MSUploadReturnDto.fromMetadata(meta, id),
          };
        } catch (e) {
          console.error(e);
          return this.catchMessage(e, { id: format.id });
        }
      }),
    );
  }
}
