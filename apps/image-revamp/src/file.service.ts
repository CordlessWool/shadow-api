import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';
import sharp from 'sharp';

@Injectable()
export class FileService {
  constructor(private readonly axios: HttpService) {}

  async save(url: string, file: Buffer, meta: sharp.Metadata): Promise<void> {
    await firstValueFrom(
      this.axios.put(url, file, {
        headers: {
          'content-type': `image/${meta.format}`,
        },
      }),
    );
  }

  async get(url: string): Promise<Buffer> {
    const res = await firstValueFrom(
      this.axios.get<Buffer>(url, { responseType: 'arraybuffer' }),
    );
    return res.data;
  }

  async getWithType(url: string): Promise<[Buffer, string]> {
    const res = await firstValueFrom(
      this.axios.get<Buffer>(url, { responseType: 'arraybuffer' }),
    );
    const header = res.headers;
    return [res.data, header['content-type']];
  }
}
