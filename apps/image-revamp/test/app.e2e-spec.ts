import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ClientProxy, ClientsModule, Transport } from '@nestjs/microservices';
import * as fs from 'fs';
import * as path from 'path';
import * as faker from 'faker';
import * as sharp from 'sharp';
import * as request from 'supertest';
import { firstValueFrom, lastValueFrom } from 'rxjs';
import { MSConvertDTO } from '../src/dto/ms-convert.dto';
import { MSUploadDto, MSUploadReturnDto } from '../src/dto/ms-upload.dto';
import { Message } from '../src/types';
import { FileService } from '../src/file.service';
import { ImageRevampModule } from '../src/image-revamp.module';

const uploadedImages: Record<string, Buffer> = {};
let storedImages: Record<string, Buffer> = {};
// TODO: generate s3 url instead of mock the service
const FileServiceMock = {
  save: (url: string, file: Buffer) => {
    uploadedImages[url] = file;
    const regex = /\/([^/]+)$/g;
    const [, name] = regex.exec(url) || [];
    if (name) {
      fs.writeFileSync(`${__dirname}/data/created/${name}`, file, {});
    }
    return Promise.resolve();
  },
  get: (url: string) => {
    if (storedImages[url]) {
      return Promise.resolve(storedImages[url]);
    }
    return Promise.reject();
  },
  getWithType: (url: string): Promise<[Buffer, string]> => {
    if (storedImages[url]) {
      return Promise.resolve([storedImages[url], 'image/jpg']);
    }
    return Promise.reject();
  },
};

jest.setTimeout(10000);

describe('ImageRevampController (e2e)', () => {
  let app: INestApplication;
  let defaultImage: Buffer;
  let noExifImage: Buffer;
  let fastImage: Buffer;
  let fastImageFormat: string;
  let pngImage: Buffer;
  let client: ClientProxy;

  beforeAll(() => {
    defaultImage = fs.readFileSync(
      path.resolve('test/data/images/default2.jpg'),
    );
    noExifImage = fs.readFileSync(path.resolve('test/data/images/no-exif.jpg'));
    fastImage = fs.readFileSync(path.resolve('test/data/images/no-exif-2.jpg'));
    fastImageFormat = 'jpg';
    pngImage = fs.readFileSync(path.resolve('test/data/images/format.png'));
  });

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        ImageRevampModule,
        ClientsModule.register([{ name: 'REVAMP', transport: Transport.TCP }]),
      ],
    })
      .overrideProvider(FileService)
      .useValue(FileServiceMock)
      .compile();

    app = module.createNestApplication();
    app.connectMicroservice({
      transport: Transport.TCP,
    });
    await app.startAllMicroservices();
    await app.init();
    client = app.get<ClientProxy>('REVAMP');
    await client.connect();
  });

  afterAll(async () => {
    await client.close();
    await app.close();
  });

  describe('request and convert', () => {
    let server;

    beforeAll(async () => {
      server = app.getHttpServer();

      // Fill storedImages
      storedImages = {
        'https://s3.test/default': defaultImage,
        'https://s3.test/no-exif': noExifImage,
        'https://s7.drop.de/png': pngImage,
      };
    });

    it('request only with src', async () => {
      const res = await request(server)
        .get('/?src=https://s3.test/no-exif')
        .expect(200);
      const image = Buffer.from(res.body);
      expect(image).toBeInstanceOf(Buffer);
      expect(image.toString('base64')).toBe(noExifImage.toString('base64'));
    }, 2500);

    it('jpeg transform', async () => {
      const res = await request(server).get(
        '?src=https://s3.test/default&blur=true&height=500&format=webp',
      );

      const image = Buffer.from(res.body);
      expect(image).toBeInstanceOf(Buffer);

      const { height, format } = await sharp(image).metadata();
      expect(height).toBe(500);
      expect(format).toBe('webp');
    });

    it('png transform', async () => {
      const res = await request(server)
        .get('?src=https://s7.drop.de/png&blur&height=500&format=webp')
        .expect(200);
      const image = Buffer.from(res.body);
      expect(image).toBeInstanceOf(Buffer);

      const { height, format } = await sharp(image).metadata();
      expect(height).toBe(500);
      expect(format).toBe('webp');
    });
  });

  describe('transform and upload', () => {
    it('return not change message', async () => {
      const id = faker.datatype.uuid();
      const res = await firstValueFrom(
        client.send<Buffer, MSUploadDto>(
          { cmd: 'transform', upload: true, multi: true },
          {
            formats: [
              {
                height: 10000,
                width: 10000,
                id,
                url: 'http://test.de/affe',
              },
            ],
            image: defaultImage.toString('base64url'),
          },
        ),
      );

      expect(res).toEqual({
        status: 'info',
        type: 'not_changed',
        data: { id },
        message: 'Revamp not necassery',
      });
    });

    it('create webp and save', async () => {
      const id = faker.datatype.uuid();
      const url = 'http://test.local/no-exif.webp';
      const res = await firstValueFrom(
        client.send<Message<MSUploadReturnDto>, MSUploadDto>(
          { cmd: 'transform', upload: true, multi: true },
          {
            formats: [
              {
                height: 1000,
                width: 1000,
                id,
                format: 'webp',
                url,
              },
            ],
            image: noExifImage.toString('base64url'),
          },
        ),
      );

      expect(res.data).toBeDefined();
      expect(res.data?.id).toBe(id);
      expect(uploadedImages[url]).toBeInstanceOf(Buffer);
    }, 15000);

    it('create different verions with one request', async () => {
      const id = faker.datatype.uuid();
      const formats = [
        {
          height: 300,
          quality: 70,
          blur: true,
          id,
          url: `http://test.de/affeb.${fastImageFormat}`,
        },
        {
          width: 300,
          id,
          url: `http://test.de/affes.${fastImageFormat}`,
        },
        {
          height: 1000,
          id,
          url: `http://test.de/affel.${fastImageFormat}`,
        },
      ];
      const res = await lastValueFrom(
        client.send<Message, MSUploadDto>(
          { cmd: 'transform', upload: true, multi: true },
          {
            formats,
            image: fastImage.toString('base64url'),
          },
        ),
      );

      expect(res.type).toBe('data');
      expect(res.status).toBe('ok');
    }, 10000);
  });

  describe('send image and tranform', () => {
    const pattern = { cmd: 'transform' };

    it('return buffer', async () => {
      const newImage = await firstValueFrom(
        client.send<Message<string>, MSConvertDTO>(pattern, {
          height: 1000,
          image: defaultImage.toString('base64url'),
        }),
      );

      expect(Buffer.from(<string>newImage.data)).toBeInstanceOf(Buffer);
    });

    it('resize image by height', async () => {
      const {
        height = 0,
        width = 0,
        size,
      } = await sharp(defaultImage).metadata();
      const newImage = await firstValueFrom(
        client.send<Message<string>, MSConvertDTO>(pattern, {
          height: 1000,
          image: defaultImage.toString('base64url'),
        }),
      );

      const relation = height / 1000;
      const newMeta = await sharp(
        Buffer.from(<string>newImage.data),
      ).metadata();

      expect(newMeta.height).toBe(1000);
      expect((newMeta.width || 0) * relation).toBeCloseTo(width, -1);
      expect(newMeta.size).toBeLessThan(size || 0);
    });

    it('resize image by height and width (landscape)', async () => {
      const length = 700;
      const {
        height = 0,
        width = 0,
        size,
      } = await sharp(defaultImage).metadata();
      const newImage = await firstValueFrom(
        client.send<Message<string>, MSConvertDTO>(pattern, {
          height: length,
          width: length,
          image: defaultImage.toString('base64url'),
        }),
      );

      const relation = height / length;
      const newMeta = await sharp(
        Buffer.from(<string>newImage.data),
      ).metadata();

      expect(newMeta.height).toBe(length);
      expect((newMeta.width || 0) * relation).toBeCloseTo(width, -1);
      expect(newMeta.size).toBeLessThan(size || 0);
    });

    it('resize image by height and width (portrait)', async () => {
      const length = 700;
      const { height = 0, width = 0, size } = await sharp(fastImage).metadata();
      const newImage = await firstValueFrom(
        client.send<Message<string>, MSConvertDTO>(pattern, {
          height: length,
          width: length,
          image: fastImage.toString('base64url'),
        }),
      );

      const relation = width / length;
      const newMeta = await sharp(
        Buffer.from(<string>newImage.data),
      ).metadata();

      expect(newMeta.width).toBe(length);
      expect((newMeta.height || 0) * relation).toBeCloseTo(height, -1);
      expect(newMeta.size).toBeLessThan(size || 0);
    });

    it('do not change', async () => {
      const size = 10000;
      const message = await firstValueFrom(
        client.send<Message<string>, MSConvertDTO>(pattern, {
          height: size,
          width: size,
          image: defaultImage.toString('base64url'),
        }),
      );
      expect(message).toEqual({
        status: 'info',
        type: 'not_changed',
        message: 'Revamp not necassery',
      });
    });
  });
});
