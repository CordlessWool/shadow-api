import { JwtUserData } from '@lib/interfaces';
import * as jwt from 'jsonwebtoken';

export const generateJwt = (secret, data: JwtUserData): string => {
  return jwt.sign(data, secret);
};
