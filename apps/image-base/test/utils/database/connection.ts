import { Connection, createConnection } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import getConfig from '@api/config/configuration';
import { DatabaseConfig } from '@api/config/types';

export class TestDatabaseConneciton {
  #schemaName: string;

  #config: DatabaseConfig;

  #conneciton: Connection;

  constructor() {
    this.#schemaName = `test_${uuidv4()}`.replace(/-/g, '');
    this.#config = getConfig().database;
  }

  get schema() {
    return this.#schemaName;
  }

  async create() {
    this.#conneciton = await createConnection({
      name: this.#schemaName,
      type: this.#config.type,
      host: this.#config.host,
      port: this.#config.port,
      username: this.#config.username,
      password: this.#config.password,
      database: 'postgres',
      dropSchema: true,
    });

    await this.#conneciton.query(`CREATE DATABASE ${this.#schemaName}`);
    await this.#conneciton.query(`SET search_path = ${this.#schemaName}`);
    // await this.#conneciton.synchronize();
  }

  async remove() {
    await this.#conneciton.query(`SET search_path = postgres`);
    await this.#conneciton.query(`DROP DATABASE ${this.#schemaName}`);
    // await this.#conneciton.dropDatabase();
    await this.#conneciton.close();
  }
}
