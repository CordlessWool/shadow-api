import { AppModule } from '@api/app.module';
import { FormatReposotory } from '@api/formats/format.reposotory';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { Role } from '@lib/enum';
import { JwtUserData } from '@lib/interfaces';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as faker from 'faker';
import * as request from 'supertest';

import { generateJwt } from './utils/generate-jwt';

const buildApp = async (): Promise<INestApplication> => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  const app = moduleFixture.createNestApplication();

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  await app.init();

  return app;
};

describe('Format overwrite tests', () => {
  let app: INestApplication;
  let jwtSecret: string;
  let jwt: string;

  beforeEach(async () => {
    process.env.FORMAT_CONFIG_LOCATION = `${__dirname}/config/format.json`;
    app = await buildApp();
    const configService = app.get(ImageConfigService);
    jwtSecret = configService.get('JWT__SECRET') || '';
    delete process.env.FORMAT_CONFIG_LOCATION;
  });

  afterEach(async () => {
    await app.close();
    // await databaseConneciton.remove();
  });

  beforeEach(() => {
    const data: JwtUserData = {
      userId: faker.datatype.uuid(),
      accountId: faker.datatype.uuid(),
      name: faker.name.findName(),
      email: faker.internet.email(),
      role: Role.admin,
    };
    jwt = generateJwt(jwtSecret, data);
  });

  describe('clean formats table', () => {
    let server;
    beforeEach(async () => {
      process.env.FORMAT_CONFIG_CLEAN = 'true';
      await app.close();
      app = await buildApp();
      app.get(FormatReposotory).wipe();
      server = app.getHttpServer();
    });

    afterEach(async () => {
      delete process.env.FORMAT_CONFIG_CLEAN;
    });

    it('no formats in table', async () => {
      const result = await request(server)
        .get('/formats')
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      const { data, total } = result.body;
      expect(data.length).toBe(0);
      expect(total).toBe(0);
    });
  });

  describe('keep table without clean or overwrite env', () => {
    let server;
    beforeEach(async () => {
      await app.close();
      app = await buildApp();
      server = app.getHttpServer();
    });

    it('still all formats exists', async () => {
      const result = await request(server)
        .get('/formats')
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      const { data } = result.body;

      expect(data).toContainEqual({
        id: 'l',
        height: 1000,
        width: 1000,
        fit: 'cover',
      });
    });
  });
});
