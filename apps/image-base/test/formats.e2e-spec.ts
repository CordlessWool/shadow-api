import { AppModule } from '@api/app.module';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { Role } from '@lib/enum';
import { JwtUserData } from '@lib/interfaces';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as faker from 'faker';
import * as request from 'supertest';

import { generateJwt } from './utils/generate-jwt';

const sleep = (time: number): Promise<void> => {
  return new Promise((res) => {
    setTimeout(res, time);
  });
};

describe('Format related tests', () => {
  let app: INestApplication;
  let jwtSecret: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    process.env.FORMAT_CONFIG_LOCATION = `${__dirname}/config/format.json`;

    app = moduleFixture.createNestApplication();

    const configService = app.get(ImageConfigService);
    jwtSecret = configService.get('JWT__SECRET') || '';

    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    await app.init();
  });

  afterAll(async () => {
    await sleep(1000);
    await app.close();
    delete process.env.FORMAT_CONFIG_LOCATION;
    // await databaseConneciton.remove();
  });

  describe('check if formats are loaded', () => {
    let server;
    let jwt: string;

    beforeAll(async () => {
      server = app.getHttpServer();
    });

    beforeEach(() => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.admin,
      };
      jwt = generateJwt(jwtSecret, data);
    });

    it('get formats', async () => {
      const result = await request(server)
        .get('/formats')
        .set('Authorization', `Bearer ${jwt}`)
        .expect(200);

      const { data } = result.body;

      expect(data).toContainEqual({
        id: 'l',
        height: 1000,
        width: 1000,
        fit: 'cover',
      });
    });
  });
});
