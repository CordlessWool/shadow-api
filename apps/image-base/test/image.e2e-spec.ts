import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@api/app.module';
import * as request from 'supertest';
import * as faker from 'faker';
import { UrlExpression, UuidExpression } from '@api/utils/regexp';
import exifr from 'exifr';
import { JwtUserData } from '@lib/interfaces';
import { Role } from '@lib/enum';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { Transport } from '@nestjs/microservices';
import { FormatReposotory } from '@api/formats/format.reposotory';
import { generateJwt } from './utils/generate-jwt';

describe('ImageController (e2e)', () => {
  let app: INestApplication;
  let jwtSecret: string;
  // let databaseConneciton: TestDatabaseConneciton;

  beforeAll(async () => {
    // databaseConneciton = new TestDatabaseConneciton();
    // await databaseConneciton.create();
    // process.env.DB_TEST = 'true';
    // process.env.DB_TEST_SCHEMA = databaseConneciton.schema;

    // console.info(databaseConneciton.schema)

    process.env.FORMAT_CONFIG_OVERWRITE = 'true';

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    const configService = app.get(ImageConfigService);
    jwtSecret = configService.get('JWT__SECRET') || '';
    app.get(FormatReposotory).wipe();
    app.connectMicroservice(
      {
        transport: Transport.TCP,
        options: {
          port: 3002,
        },
      },
      { inheritAppConfig: true },
    );
    await app.startAllMicroservices();

    await app.init();
  });

  afterAll(async () => {
    await app.close();
    // await databaseConneciton.remove();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/images').expect(200);
  });

  describe('test CRUD routes', () => {
    let server;
    let jwt;

    beforeAll(async () => {
      server = app.getHttpServer();
    });

    beforeEach(() => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(jwtSecret, data);
    });

    it('upload image without exif data', async () => {
      const noExifFile = 'test/data/images/no-exif.jpg';
      const date = Date.now();
      const result = await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        .attach('no-exif', noExifFile)
        .expect(201);

      const id = result.body[0];
      expect(UuidExpression.setValue(id).isValid()).toBe(true);

      const resultData = await request(server)
        .get(`/images/${id}/meta`)
        .expect(200);
      expect(resultData.body.id).toEqual(id);
      expect(new Date(resultData.body.createdAt).getTime()).toBeGreaterThan(
        date,
      );
      expect(resultData.body.gps).toBeUndefined();
    }, 20000);
  });

  describe('test CRUD routes with default upload', () => {
    const defaultFile = 'test/data/images/default.jpg';
    let imageName: string;
    let uuid: string;
    let server;
    let jwt: string;

    beforeAll(() => {
      server = app.getHttpServer();
    });

    beforeEach(async () => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(jwtSecret, data);

      imageName =
        Math.random() > 0.5 ? faker.lorem.word() : faker.lorem.words();
      const result = await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        .attach(imageName, defaultFile)
        .expect(201);
      [uuid] = result.body;
      expect(UuidExpression.setValue(uuid).isValid()).toBe(true);
    });

    it('/get/:id should not find random id', () => {
      return request(app.getHttpServer())
        .get(`/images/${faker.datatype.uuid()}`)
        .expect(404);
    }, 1000);

    it('check default image upload', async () => {
      const res = await request(app.getHttpServer())
        .get(`/images/${uuid}`)
        .expect(303);
      expect(UrlExpression.setValue(res.headers.location).isValid()).toBe(true);
    }, 2000);

    it('check default test data', async () => {
      const resultData = await request(server)
        .get(`/images/${uuid}/meta`)
        .expect(200);
      expect(resultData.body.id).toEqual(uuid);
      expect(resultData.body.name).toEqual(imageName);

      const imageData = await exifr.parse(defaultFile);
      expect(new Date(resultData.body.createdAt).toString()).toBe(
        imageData.DateTimeOriginal.toString(),
      );
    }, 2000);

    it('patch data', async () => {
      const lat = 50.29960277777778;
      const lon = 14.820294444444444;

      await request(server)
        .patch(`/images/${uuid}/meta`)
        .set('Authorization', `Bearer ${jwt}`)
        .send({
          lat,
          lon,
        })
        .expect(200);

      // expect(UuidExpression.setValue(patchResult.text).isValid()).toBe(true);

      const resultData = await request(server)
        .get(`/images/${uuid}/meta`)
        .expect(200);
      // console.log(resultData.body);
      expect(resultData.body.gps).toEqual({ lat, lon });
    });

    // it('remove', async () => {
    //   await sleep(1500);
    //   // TODO: delete list from queer if image is deleted
    //   const resultData = await request(server).get(`/images`).expect(200);
    //   const amount = resultData.body.total;
    //   expect(amount).toBeGreaterThanOrEqual(1);

    //   await request(server).delete(`/images/${uuid}`).expect(200);

    //   const secondResult = await request(server).get(`/images`).expect(200);
    //   expect(secondResult.body.total).toBe(amount - 1);
    // });
  });
});
