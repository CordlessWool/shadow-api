import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import * as faker from 'faker';
import { RegExpFactory } from '@api/utils/regexp';
import { DefaultSizes, Role } from '@lib/enum';
import { JwtUserData } from '@lib/interfaces';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { FormatReposotory } from '@api/formats/format.reposotory';
import { AppModule } from '../src/app.module';
import { generateJwt } from './utils/generate-jwt';

type tagData = { name: string; description: string; color: string };
const generateUniqueName = (str?: string): string => {
  return `${str || faker.lorem.word()}-${faker.datatype.uuid()}`.slice(
    0,
    DefaultSizes.NAME,
  );
};
const generateRandomTagData = (amount: number): Partial<tagData>[] => {
  const tagData: Partial<tagData>[] = [];
  for (let i = 0; i < amount; i += 1) {
    const fakeData: Partial<tagData> = {
      name: generateUniqueName(),
      description: faker.lorem.words().slice(0, DefaultSizes.DESCRIPTION),
      color: '#AE73EE',
    };
    tagData.push(fakeData);
  }
  return tagData;
};

describe('TagController (e2e)', () => {
  let app: INestApplication;
  let jwt: string;
  let jwtSecret: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    const configService = app.get(ImageConfigService);
    app.get(FormatReposotory).wipe();
    jwtSecret = configService.get('JWT__SECRET') || '';

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  beforeEach(() => {
    const data: JwtUserData = {
      userId: faker.datatype.uuid(),
      accountId: faker.datatype.uuid(),
      name: faker.name.findName(),
      email: faker.internet.email(),
      role: Role.editor,
    };
    jwt = generateJwt(jwtSecret, data);
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/tags').expect(200);
  });

  describe('limits of tags api', () => {
    let req;

    beforeEach(() => {
      const server = app.getHttpServer();
      req = request(server);
    });

    it('exide max length of description', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.words(DefaultSizes.DESCRIPTION + 7),
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });

    it('maxLength ist description size', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem
          .words(DefaultSizes.DESCRIPTION)
          .slice(0, DefaultSizes.DESCRIPTION),
      };
      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(201);
    });

    it('color code only hex', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.word(),
        color: '255, 255, 255',
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });

    it('color code only hex signes', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.word(),
        color: '#ZZZ',
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });

    it('color code require #', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.word(),
        color: 'EEE',
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });

    it('color code only hex with 3 or 6 digets', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.word(),
        color: '#5555',
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });

    it('color code with more then 6 digits is wrong', async () => {
      const tag = {
        name: generateUniqueName(),
        description: faker.lorem.word(),
        color: '#5543EEE',
      };

      await req
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(tag)
        .expect(400);
    });
  });

  describe('Test base functionalty of tags api', () => {
    let testTag: Record<string, unknown>;
    let uuid: string;
    let server;
    let expectedValue: unknown;

    beforeEach(async () => {
      testTag = {
        name: generateUniqueName(),
        description: faker.lorem.words().slice(0, DefaultSizes.DESCRIPTION),
        color: '#777',
      };
      server = app.getHttpServer();
      const postData = await request(server)
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send(testTag)
        .expect(201);

      uuid = postData.text;
      expect(RegExpFactory.expression('UUID').setValue(uuid).isValid()).toBe(
        true,
      );
    });

    afterEach(async () => {
      if (expectedValue !== undefined) {
        const res = await request(app.getHttpServer())
          .get(`/tags/${uuid}`)
          .expect(200);

        expect(res.body).toEqual(expectedValue);

        expectedValue = undefined;
      }
    });

    it('get', async () => {
      expectedValue = {
        id: uuid,
        permission: 'public',
        imageIds: [],
        ...testTag,
      };
    });

    it('search', async () => {
      const testData = generateRandomTagData(10);
      const uniqueData = {
        name: generateUniqueName('unique'),
        description: 'Some text taht include the word unique',
        color: faker.internet.color(),
      };
      testData.push(uniqueData);
      const promises = testData.map((data) => {
        return request(server)
          .post('/tags')
          .set('Authorization', `Bearer ${jwt}`)
          .send(data)
          .expect(201);
      });
      const results = await Promise.all(promises);

      const searchResult = await request(server)
        .get('/tags')
        .query({
          search: uniqueData.name.slice(3, -4),
        });

      expect(searchResult.body.data).toContainEqual({
        id: results[results.length - 1].text,
        imageIds: [],
        ...uniqueData,
      });
    });

    it('find', async () => {
      const testData = generateRandomTagData(5);

      const promises = testData.map((data) => {
        return request(server)
          .post('/tags')
          .set('Authorization', `Bearer ${jwt}`)
          .send(data)
          .expect(201);
      });
      const postResults = await Promise.all(promises);
      const result = await request(server)
        .get('/tags')
        .query({
          name: testData[1].name,
        })
        .expect(200);

      expect(result.body.data[0]).toEqual({
        ...testData[1],
        imageIds: [],
        id: postResults[1].text,
      });
      expect(result.body.total).toBe(1);
    });

    it('update', async () => {
      const changedTag = {
        description: 'some new description',
        color: '#FF7',
      };

      await request(server)
        .put(`/tags/${uuid}`)
        .set('Authorization', `Bearer ${jwt}`)
        .send(changedTag)
        .expect(200);

      expectedValue = {
        id: uuid,
        permission: 'public',
        imageIds: [],
        ...testTag,
        ...changedTag,
      };
    });

    it('remove', async () => {
      await request(server)
        .del(`/tags/${uuid}`)
        .set('Authorization', `Bearer ${jwt}`)
        .send()
        .expect(204);

      await request(app.getHttpServer()).get(`/tags/${uuid}`).expect(404);
    });
  });
});
