import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@api/app.module';
import * as request from 'supertest';
import * as faker from 'faker';
import { UuidExpression } from '@api/utils/regexp';
import { Transport } from '@nestjs/microservices';
import { JwtUserData } from '@lib/interfaces';
import { Role } from '@lib/enum';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { ImageStatus } from '@api/images/defines/image-status.enum';
import { Image } from '@api/images/entities/image.entity';
import { generateJwt } from './utils/generate-jwt';

const sleep = (time: number): Promise<void> => {
  return new Promise((res) => {
    setTimeout(res, time);
  });
};

describe('ImageController (e2e)', () => {
  let app: INestApplication;
  let jwtSecret: string;
  // let databaseConneciton: TestDatabaseConneciton;

  beforeAll(async () => {
    process.env.FORMAT_CONFIG_LOCATION = `${__dirname}/config/format.json`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    const configService = app.get(ImageConfigService);
    jwtSecret = configService.get('JWT__SECRET') || '';

    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    app.connectMicroservice(
      {
        transport: Transport.TCP,
      },
      { inheritAppConfig: true },
    );
    await app.startAllMicroservices();

    await app.init();
  });

  afterAll(async () => {
    await sleep(1500);
    await app.close();
    delete process.env.FORMAT_CONFIG_LOCATION;
    // await databaseConneciton.remove();
  });

  describe('test CRUD routes with default upload', () => {
    const defaultFile = 'test/data/images/default.jpg';
    let imageName: string;
    let uuid: string;
    let server;
    let jwt: string;

    beforeAll(() => {
      server = app.getHttpServer();
    });

    beforeEach(async () => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(jwtSecret, data);

      imageName =
        Math.random() > 0.5 ? faker.lorem.word() : faker.lorem.words();
      const result = await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        .attach(imageName, defaultFile)
        .expect(201);
      [uuid] = result.body;
      expect(UuidExpression.setValue(uuid).isValid()).toBe(true);
    });

    it('get Formats of Image', async () => {
      const result = await request(server)
        .get(`/images/${uuid}/formats`)
        .expect(200);

      expect(result.body.total).toBe(3);
      expect(result.body.data.length).toBe(3);

      const data = result.body.data.sort((f, s) => {
        if (!f.formatRefName) return -1;
        if (!s.formatRefName) return 1;
        const a = f.formatRefName.toUpperCase();
        const b = s.formatRefName.toUpperCase();

        if (a < b) return -1;
        if (a > b) return 1;

        return 0;
      });

      const keys = Object.keys(data[1]).sort();
      const expectedKeys = [
        'formatType',
        'formatRefName',
        'width',
        'height',
        'status',
        'size',
      ].sort();
      expect(keys).toEqual(expectedKeys);

      expect(data[2].formatRefName).toBe('s');
      expect(data[2].height).toBe(300);

      expect(data[0].formatRefName).toBe('l');
      expect(data[0].height).toBe(1000);
    }, 5000);

    it('check if status is set to complete', async () => {
      let formatCreating;
      let result;
      const sleeptime = 1000;
      do {
        result = await request(server)
          .get(`/images/${uuid}/formats`)
          .expect(200);
        expect(result.body.data.length).toBe(3);
        expect(result.body.total).toBe(3);
        formatCreating = result.body.data.find(
          (i: Image) => i.status !== ImageStatus.complete,
        );
        console.info(
          `image formats are not created yet, wait ${sleeptime}ms more`,
        );
        await sleep(sleeptime);
      } while (formatCreating != null);

      expect(formatCreating).toBeUndefined();
      expect(result.body.data[1].status).toBe(ImageStatus.complete);
    }, 10000);

    // it('remove', async () => {
    //   await sleep(1500);
    //   // TODO: delete list from queer if image is deleted
    //   const resultData = await request(server).get(`/images`).expect(200);
    //   const amount = resultData.body.total;
    //   expect(amount).toBeGreaterThanOrEqual(1);

    //   await request(server).delete(`/images/${uuid}`).expect(200);

    //   const secondResult = await request(server).get(`/images`).expect(200);
    //   expect(secondResult.body.total).toBe(amount - 1);
    // });
  });

  describe('deep test findOne', () => {
    const defaultFile = 'test/data/images/default.jpg';
    let imageName: string;
    let uuid: string;
    let server;
    let jwt: string;

    // beforeAll(() => {
    //   imageBinary = fs.readFileSync(defaultFile);
    // })

    beforeEach(async () => {
      server = app.getHttpServer();

      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(jwtSecret, data);

      imageName =
        Math.random() > 0.5 ? faker.lorem.word() : faker.lorem.words();
      const result = await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        .attach(imageName, defaultFile)
        .expect(201);
      [uuid] = result.body;
      expect(UuidExpression.setValue(uuid).isValid()).toBe(true);
    });

    it('check for different redirect urls', async () => {
      const resOriginal = await request(server)
        .get(`/images/${uuid}?ref=original`)
        .expect(303);

      const originalUrl = resOriginal.headers.location;
      const resSmall = await request(server)
        .get(`/images/${uuid}?height=300`)
        .expect(303);
      const smallUrl = resSmall.headers.location;
      expect(originalUrl).not.toBe(smallUrl);

      const resLarge = await request(server)
        .get(`/images/${uuid}?width=1000`)
        .expect(303);
      const largeUrl = resLarge.headers.location;

      expect(originalUrl).not.toBe(largeUrl);
      expect(largeUrl).not.toBe(smallUrl);
    });

    // it('check default image upload', async () => {
    //   const res = await request(app.getHttpServer()).get(`/images/${uuid}`).expect(303);
    //   const redirectUrl = res.headers['location'];
    //   const image = await axios.get(redirectUrl);
    //   console.log(image)
    //   expect(image).toEqual(imageBinary);
    // })
  });
});
