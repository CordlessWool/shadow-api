import { AppModule } from '@api/app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';

import * as faker from 'faker';
import { Role } from '@lib/enum';
import { JwtUserData } from '@lib/interfaces';
import { Transport } from '@nestjs/microservices';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { generateJwt } from './utils/generate-jwt';

describe('Test Authentication (e2e)', () => {
  let app: INestApplication;
  let jwtSecret: string;
  let server;
  let jwt: string;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    const configService = app.get(ImageConfigService);
    jwtSecret = configService.get('JWT__SECRET') || '';
    app.connectMicroservice(
      {
        transport: Transport.TCP,
        options: {
          port: 3001,
        },
      },
      { inheritAppConfig: true },
    );
    await app.startAllMicroservices();
    await app.init();

    server = app.getHttpServer();
  });

  afterAll(async () => {
    await app.close();
  });

  it('can reach unprotected routes', () => {
    return request(server).get('/images').expect(200);
  });

  describe('as photograpge', () => {
    beforeEach(() => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(jwtSecret, data);
    });

    it('JWT Required before validate content', async () => {
      await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        // .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(400);
    });

    it('Role is checked', async () => {
      await request(server)
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send({ name: faker.lorem.word() })
        .expect(403);
    });

    it('Fail without JWT in header', async () => {
      const uuid = faker.datatype.uuid();
      await request(app.getHttpServer())
        .post('/images')
        .set('Authorization', `Bearer ${uuid}`)
        .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(401);
    });

    it('Fail without Bearer identifier', async () => {
      await request(server)
        .post('/images')
        .set('Authorization', `${jwt}`)
        .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(401);
    });

    it('Do not accept expired JWT', async () => {
      await request(server)
        .post('/images')
        .set(
          'Authorization',
          `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoib3duZXIiLCJpZCI6ImQzZDc0NjYwLTc3ODMtNGY4OC05ZWY5LTAwN2EzMzQyNmE4ZCIsImFjY291bnQiOiIwMmJlZmZlZS02NTRiLTQxNDctODY0ZC1mYmIzMWYwMTE4MTIiLCJuYW1lIjoiTWFuZHkgQmVpZXIiLCJlbWFpbCI6IldpbGx5X1Jvb2I2NEBnbWFpbC5jb20iLCJpYXQiOjE2MzA1MzY5NTIsImV4cCI6MTYzMDUzNzM3Mn0.N03BV6Bc7PqZM7ZyikWvy8fmlK6aq_0P07ijbtRqc4c`,
        )
        .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(401);
    });

    it('Do not accept with wrong secrept', async () => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.photographer,
      };
      jwt = generateJwt(faker.random.words(), data);

      await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(401);
    });
  });

  describe('as editor', () => {
    beforeEach(() => {
      const data: JwtUserData = {
        userId: faker.datatype.uuid(),
        accountId: faker.datatype.uuid(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        role: Role.editor,
      };
      jwt = generateJwt(jwtSecret, data);
    });

    it('Role hirarchie', async () => {
      await request(server)
        .post('/images')
        .set('Authorization', `Bearer ${jwt}`)
        // .attach('no-exif', 'test/data/images/default-xxs.jpg')
        .expect(400);
    });

    it('JWT Required', async () => {
      await request(server)
        .post('/tags')
        .set('Authorization', `Bearer ${jwt}`)
        .send({ name: faker.lorem.word() })
        .expect(201);
    });
  });
});
