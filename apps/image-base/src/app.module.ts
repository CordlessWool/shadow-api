import { APP_GUARD } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImagesModule } from './images/images.module';
import { StorageModule } from './storage/storage.module';
import { TagsModule } from './tags/tags.module';
import { FormatsModule } from './formats/formats.module';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { ImageConfigModule } from './image-config/image-config.module';
import { ImageConfigService } from './image-config/image-config.service';

const modules = {
  imports: [
    ImageConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [ImageConfigService],
      useFactory: (configService: ImageConfigService) => {
        const type = configService.get('DATABASE__TYPE');
        const host = configService.get('DATABASE__HOST');
        const port = configService.get('DATABASE__PORT');
        console.info(`Connecting ${type} database on ${host}:${port}`);
        let database = configService.get('DATABASE__NAME');
        let schema: string | undefined = configService.get('DATABASE__IMAGE');
        if (database == null) {
          database = schema;
          schema = undefined;
        }

        return {
          type,
          host,
          port,
          username: configService.get('DATABASE__USERNAME'),
          password: configService.get('DATABASE__PASSWORD'),
          database,
          schema,
          entities: [`${__dirname}/**/*.entity{.ts,.js}`],
          ssl: configService.get('DATABASE__SSL'),
          synchronize: true,
          keepConnectionAlive: false,
          autoLoadEntities: true,
          verboseRetryLog: true,
          logging: ['error', 'info', 'warn', 'log'],
          logger: 'debug',
        };
      },
      inject: [ImageConfigService],
    }),
    ImagesModule,
    StorageModule,
    TagsModule,
    FormatsModule,
    AuthModule,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
};
@Module(modules)
export class AppModule {}
