import { ValidationExeption } from '@lib/exeptions/validation.exeption';
import { plainToClass } from 'class-transformer';
import { ValidateNested, validateSync } from 'class-validator';
import { FormatConfig } from './format.config';

class Wrapper {
  @ValidateNested({ each: true })
  formats: FormatConfig[];
}

export const formatValidate = (
  config: Record<string, unknown>[],
): FormatConfig[] => {
  const formats = plainToClass(FormatConfig, config, {
    enableImplicitConversion: true,
  });

  const wrapper = new Wrapper();
  wrapper.formats = formats;

  const errors = validateSync(wrapper, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw ValidationExeption.fromClassValidator(errors);
  }
  return formats;
};
