import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import * as faker from 'faker';
import { FindManyOptions } from 'typeorm';
import { FormatFit } from '@lib/enum';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { FormatsService } from './formats.service';
import { Format } from './entities/format.entity';
import { FormatReposotory } from './format.reposotory';

const dummyLocation = './local.conf.json';
class DatabaseMock {
  private data: Array<Format> = [];

  private getIndex(id) {
    return this.data.findIndex((value) => value.id === id);
  }

  hasElements() {
    return this.data.length !== 0;
  }

  save(_format: Format) {
    const format = { ..._format };
    if (!format.id) format.id = faker.lorem.word();
    if (!format.createdAt) format.createdAt = new Date();
    this.data = [...this.data, format];
  }

  update(id, format) {
    this.data = this.data.map((value) => {
      if (value.id === id) {
        return {
          ...value,
          updatedAt: new Date(),
          ...format,
        };
      }
      return value;
    });
  }

  remove(format: Format) {
    const index = this.getIndex(format.id);
    this.data.splice(index, 1);
  }

  findOne(id) {
    return this.data.find((value) => value.id === id);
  }

  find(options?: FindManyOptions): Format[] {
    if (options?.order) {
      this.data.sort((a, b) => {
        const aDate = a.updatedAt || a.createdAt;
        const bDate = b.updatedAt || b.createdAt;

        return bDate.getTime() - aDate.getTime();
      });
    }
    return this.data;
  }

  reset() {
    this.data = [];
  }
}

const mockConifigLocation = () => {
  jest.mock(
    dummyLocation,
    () => [
      {
        name: faker.lorem.word(),
        width: 40,
      },
      {
        name: faker.lorem.word(),
        height: 30,
        fit: FormatFit.contain,
      },
    ],
    { virtual: true },
  );
};

const createModuleForTesting = async () => {
  process.env.FORMAT_CONFIG_LOCATION = dummyLocation;
  const module = await Test.createTestingModule({
    imports: [],
    providers: [
      FormatsService,
      {
        provide: ImageConfigService,
        useClass: ConfigService,
      },
      {
        provide: FormatReposotory,
        useValue: new DatabaseMock(),
      },
    ],
  }).compile();

  mockConifigLocation();

  return module;
};

it('FormatsService Bootstrap', async () => {
  const module: TestingModule = await createModuleForTesting();
  const service = module.get<FormatsService>(FormatsService);
  const repo = module.get<FormatReposotory>(FormatReposotory);

  const spy = jest.spyOn(repo, 'save');

  await service.onApplicationBootstrap();

  expect(spy).toBeCalled();
});

describe('FormatsService', () => {
  let service: FormatsService;
  let database: FormatReposotory;

  beforeEach(async () => {
    const module: TestingModule = await createModuleForTesting();

    service = module.get<FormatsService>(FormatsService);
    database = module.get<FormatReposotory>(FormatReposotory);
    await service.onApplicationBootstrap();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('test config is saved', async () => {
    const formats = await import(dummyLocation);
    expect(formats).toBeDefined();
    const all = await database.find();
    expect(all.length).toBe(formats?.length);
    const names = formats?.map((format) => format.name);
    const ids = all.map((format) => format.id);

    expect(names).toEqual(ids);
  });
});
