import { EntityRepository, Repository } from 'typeorm';
import { Format } from './entities/format.entity';

@EntityRepository(Format)
export class FormatReposotory extends Repository<Format> {
  async hasElements() {
    return (await this.count()) !== 0;
  }

  async wipe() {
    await this.clear();
  }
}
