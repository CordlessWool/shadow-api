import { FormatFit } from '@lib/enum';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

export class FormatConfig {
  @IsString()
  name: string;

  @IsNumber()
  @IsOptional()
  height?: number;

  @IsNumber()
  @IsOptional()
  width?: number;

  @IsEnum(FormatFit)
  @IsOptional()
  fit?: FormatFit;
}
