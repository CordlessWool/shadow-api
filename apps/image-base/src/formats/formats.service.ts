import { Injectable } from '@nestjs/common';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { DataResponse } from '@lib/types';
import { CreateFormatDto } from './dto/create-format.dto';
import { UpdateFormatDto } from './dto/update-format.dto';
import { Format } from './entities/format.entity';
import { formatId } from './types';
import { formatValidate } from './format.validation';
import { FormatReposotory } from './format.reposotory';
import { FormatConfig } from './format.config';

@Injectable()
export class FormatsService {
  private formatBuffer: Array<Format> = [];

  private updated: boolean;

  constructor(
    private readonly config: ImageConfigService,
    private readonly formatsRepo: FormatReposotory,
  ) {}

  private async writeFormatsToDatabase(overwrite = false) {
    try {
      const path = this.config.get('FORMAT_CONFIG_LOCATION');
      if (typeof path === 'string') {
        console.info(`load configs from ${path}`);
        const data: Record<string, unknown>[] = await import(path);
        const formatsFromConfig: FormatConfig[] = formatValidate(data);

        if (overwrite) {
          await this.formatsRepo.clear();
        } else if (await this.formatsRepo.hasElements()) {
          return;
        }

        const promises = formatsFromConfig.map((formatConfigElement) => {
          const format = Format.fromConfig(formatConfigElement);
          return this.formatsRepo.save(format);
        });

        await Promise.all(promises);
      }
    } catch (err) {
      console.error(err);
    }
  }

  async onApplicationBootstrap() {
    const clear = this.config.get('FORMAT_CONFIG_CLEAN');
    if (clear === true) {
      await this.formatsRepo.clear();
    }
    const overwrite = this.config.get('FORMAT_CONFIG_OVERWRITE');
    await this.writeFormatsToDatabase(overwrite);
  }

  private async updateBufferIfNecassery() {
    if (this.updated || this.formatBuffer.length === 0) {
      this.formatBuffer = await this.formatsRepo.find({
        select: ['id', 'height', 'width', 'fit', 'type'],
      });
      this.updated = false;
    }
  }

  async create(data: CreateFormatDto): Promise<formatId> {
    const format = Format.fromDTO(data);
    await this.formatsRepo.save(format);
    this.updated = true;
    return format.id;
  }

  async update(id: formatId, _data: UpdateFormatDto): Promise<formatId> {
    const { id: removed, ...data } = _data;
    await this.formatsRepo.update(id, data);
    this.updated = true;
    return id;
  }

  async remove(id: formatId): Promise<void> {
    const format = await this.formatsRepo.findOne(id);
    if (format) {
      await this.formatsRepo.remove([format]);
      this.updated = true;
    }
  }

  async bufferedFind(): Promise<Format[]> {
    await this.updateBufferIfNecassery();
    return this.formatBuffer;
  }

  async findAll(): DataResponse<Format> {
    // TODO: reduce amount of selected columns
    const [data, total] = await this.formatsRepo.findAndCount({
      select: ['id', 'height', 'width', 'fit'],
    });
    return {
      data,
      total,
    };
  }
}
