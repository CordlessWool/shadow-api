import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import { FormatFit, FormatType } from '@lib/enum';
import { formatId } from '../types';
import { FormatConfig } from '../format.config';
import { CreateFormatDto } from '../dto/create-format.dto';

@Entity()
@Index(['height', 'width', 'fit', 'type'], { unique: true })
export class Format {
  @PrimaryColumn({
    type: 'varchar',
    length: 50,
  })
  id: formatId;

  @Column({
    type: 'int',
    nullable: true,
  })
  height?: number;

  @Column({
    type: 'int',
    nullable: true,
  })
  width?: number;

  @Column({
    type: 'enum',
    enum: FormatFit,
    default: FormatFit.cover,
  })
  fit: FormatFit;

  @Column({
    type: 'enum',
    enum: FormatType,
    nullable: true,
  })
  type: FormatType;

  @Column({
    default: false,
  })
  setByConfig: boolean;

  @Column({
    default: false,
  })
  setByUsage: boolean;

  @Column({
    default: 0,
  })
  requests: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  static fromConfig(data: FormatConfig): Format {
    const format = new Format();
    format.id = data.name;
    format.height = data.height;
    format.width = data.width;
    format.fit = data.fit || FormatFit.cover;

    return format;
  }

  static fromDTO(data: CreateFormatDto): Format {
    const format = new Format();
    format.id = data.id;
    format.height = data.height;
    format.width = data.width;
    format.fit = data.fit || FormatFit.cover;
    format.setByConfig = false;
    return format;
  }
}
