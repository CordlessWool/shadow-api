import { ManipulationInterface } from '@lib/interfaces';
import { FormatFit, FormatType } from '@lib/enum';

export type formatId = string;

export interface ImageManipulationInterface extends ManipulationInterface {
  height?: number;
  width?: number;
  fit?: FormatFit;
  type?: FormatType;
  id: formatId;
}
