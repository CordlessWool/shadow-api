import { FormatFit } from '@lib/enum';
import * as faker from 'faker';
import 'reflect-metadata'; // Needed for class-tranformer in formatValidate
import { plainToClass } from 'class-transformer';
import { ValidationExeption } from '@lib/exeptions/validation.exeption';
import { FormatConfig } from './format.config';
import { formatValidate } from './format.validation';

describe('validation of format data', () => {
  it('valid data', () => {
    const data: Array<Record<string, unknown>> = [
      {
        name: faker.lorem.word(),
        width: 40,
      },
      {
        name: faker.lorem.word(),
        height: 30,
        fit: FormatFit.contain,
      },
    ];

    const formats = formatValidate(data);
    expect(formats).toStrictEqual(plainToClass(FormatConfig, data));
  });

  it('name is required', () => {
    try {
      const data: Array<Record<string, unknown>> = [
        {
          name: faker.lorem.word(),
          width: 40,
        },
        {
          height: 30,
          fit: FormatFit.contain,
        },
      ];

      formatValidate(data);
      expect('should not be reeached').toBeUndefined();
    } catch (error) {
      expect(error).toBeInstanceOf(ValidationExeption);
    }
  });
});
