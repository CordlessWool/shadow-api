import { FormatFit, FormatType } from '@lib/enum';
import { formatId, ImageManipulationInterface } from '../types';

export class CreateFormatDto implements ImageManipulationInterface {
  id: formatId;

  height?: number;

  width?: number;

  fit?: FormatFit;

  type?: FormatType;
}

export class CreateFormatObject extends CreateFormatDto {
  setByConfig?: boolean;

  setByUsage?: boolean;
}
