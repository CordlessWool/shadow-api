import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FormatsService } from './formats.service';
import { FormatsController } from './formats.controller';
import { Format } from './entities/format.entity';
import { FormatReposotory } from './format.reposotory';

@Module({
  imports: [TypeOrmModule.forFeature([Format, FormatReposotory])],
  controllers: [FormatsController],
  providers: [FormatsService],
  exports: [FormatsService],
})
export class FormatsModule {}
