import { ManagementRoles, Roles } from '@api/auth/role.decorator';
import { Role } from '@lib/enum';
import { Controller, Get } from '@nestjs/common';
import { FormatsService } from './formats.service';

@ManagementRoles()
@Controller('formats')
export class FormatsController {
  constructor(private readonly formatsService: FormatsService) {}

  @Roles(Role.admin)
  @Get()
  findAll() {
    return this.formatsService.findAll();
  }
}
