import { AfterLoad } from 'typeorm';

export class EntityBase {
  @AfterLoad()
  removeNullValues() {
    const keys = Object.keys(this);
    for (const key of keys) {
      // eslint-disable-next-line security/detect-object-injection
      if (this[key] === null && this.hasOwnProperty(key)) {
        // eslint-disable-next-line security/detect-object-injection
        this[key] = undefined;
      }
    }
  }
}
