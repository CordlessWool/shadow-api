import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ImageConfigService } from './image-config/image-config.service';

async function bootstrap() {
  console.info('start api service');
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  const configService = app.get(ImageConfigService);

  app.connectMicroservice(
    {
      transport: Transport.TCP,
    },
    { inheritAppConfig: true },
  );
  await app.startAllMicroservices();
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const config = new DocumentBuilder()
    .setTitle('Image Management Service')
    .setDescription('The image management API description')
    .setVersion('0.1')
    // .addTag('cats')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(configService.get('PORT'));
}
bootstrap();
