import { Test, TestingModule } from '@nestjs/testing';
import { ImageConfigService } from './image-config.service';

describe('ImageConfigService', () => {
  let service: ImageConfigService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ImageConfigService],
    }).compile();

    service = module.get<ImageConfigService>(ImageConfigService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
