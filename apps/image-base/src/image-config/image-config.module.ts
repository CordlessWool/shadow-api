import { DynamicModule, Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigModuleOptions } from '@nestjs/config';
import { validate } from './env.validation';
import { ImageConfigService } from './image-config.service';

const prepareModule = (options: Omit<ConfigModuleOptions, 'validate'> = {}) => {
  return {
    imports: [
      ConfigModule.forRoot({
        validate,
        ...options,
      }),
    ],
    providers: [ImageConfigService],
    exports: [ImageConfigService],
  };
};

@Global()
@Module(prepareModule())
export class ImageConfigModule {
  static forRoot(
    options: Omit<ConfigModuleOptions, 'validate'> = {},
  ): DynamicModule {
    return {
      module: ImageConfigModule,
      global: true,
      ...prepareModule(options),
    };
  }
}
