import 'reflect-metadata';
import { EnvironmentVariables } from './env.definition';
import { validate } from './env.validation';

describe('test validation for image-config', () => {
  it('test transform and validation', () => {
    const envs = {
      NODE_ENV: 'development',
      SERVICE__NATS: 'nats://nats:4222',
      SERVICE__REVAMP: 'http://image-revamp:3010',
      SERVICE__ACCOUNT: 'http://localhost',
      SERVICE__IMAGE: 'http://localhost',
      DATABASE__TYPE: 'postgres',
      DATABASE__HOST: 'db-image',
      DATABASE__PORT: '5432',
      DATABASE__SSL: 'false',
      DATABASE__USERNAME: 'postgres',
      DATABASE__PASSWORD: 'root',
      DATABASE__ACCOUNT: 'shadow_account',
      DATABASE__IMAGE: 'shadow_image',
      STORAGE__TYPE: 's3',
      STORAGE__NAME: 's3',
      STORAGE__FOLDER: 'shadow',
      STORAGE__SSL: 'true',
      STORAGE__USER: 'test',
      STORAGE__SECRET: 'api-test-secret',
      STORAGE__URL: 'http://s3:9000',
      JWT__SECRET: 'someSecret',
    };

    const envOpject = validate(envs);

    expect(envOpject).toBeInstanceOf(EnvironmentVariables);
    expect(envOpject.DATABASE__SSL).toBe(false);
    expect(envOpject.DATABASE__PORT).toBe(5432);
  });
});
