/* eslint-disable @typescript-eslint/no-inferrable-types */
import { NodeEnv } from '@lib/enum';
import { Transform } from 'class-transformer';

import {
  IsBoolean,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
} from 'class-validator';

const urlBaseDefiniton = {
  require_tld: false,
  require_protocol: true,
  require_port: false,
};

export enum DatabaseTypeEnum {
  mysql = 'mysql',
  mariadb = 'mariadb',
  postgres = 'postgres',
}

export class EnvironmentVariables {
  @IsEnum(NodeEnv)
  NODE_ENV: NodeEnv;

  @IsUrl({ require_tld: false })
  HOST: string = 'http://localhost';

  @IsNumber()
  PORT: number = 3010;

  @IsString()
  @IsOptional()
  FORMAT_CONFIG_LOCATION?: string;

  @Transform(({ obj }) => obj.FORMAT_CONFIG_OVERWRITE === 'true')
  @IsBoolean()
  FORMAT_CONFIG_OVERWRITE: boolean = false;

  @Transform(({ obj }) => obj.FORMAT_CONFIG_CLEAN === 'true')
  @IsBoolean()
  FORMAT_CONFIG_CLEAN: boolean = false;

  @IsUrl({
    protocols: ['nats', 'http', 'https'],
    ...urlBaseDefiniton,
  })
  SERVICE__NATS: string;

  @IsOptional()
  @IsUrl(urlBaseDefiniton)
  SERVICE__REVAMP: string;

  @IsUrl(urlBaseDefiniton)
  SERVICE__IMAGE: string;

  @IsUrl(urlBaseDefiniton)
  SERVICE__ACCOUNT: string;

  /**
   *
   * SQL-Database
   *
   */

  @IsEnum(DatabaseTypeEnum)
  DATABASE__TYPE: keyof typeof DatabaseTypeEnum;

  @IsUrl({
    ...urlBaseDefiniton,
    require_protocol: false,
  })
  DATABASE__HOST: string;

  @IsNumber()
  DATABASE__PORT: number;

  @Transform(({ obj }) => obj.DATABASE__SSL === 'true')
  @IsBoolean()
  DATABASE__SSL: boolean = true;

  @IsString()
  DATABASE__USERNAME: string;

  @IsString()
  DATABASE__PASSWORD: string;

  @IsOptional()
  @IsString()
  DATABASE__NAME?: string;

  @IsString()
  DATABASE__ACCOUNT: string;

  @IsString()
  DATABASE__IMAGE: string;

  /**
   *
   * Redis
   *
   */
  // @IsString()
  // REDIS__HOST: string;

  // @IsNumber()
  // REDIS__PORT: number;

  // @IsString()
  // REDIS__DB: number = 0;

  // @IsString()
  // REDIS__USERNAME: string;

  // @IsString()
  // REDIS__PASSWORD: string;

  /**
   *
   * Storage
   *
   */

  @IsString()
  STORAGE__TYPE: string;

  @IsString()
  STORAGE__NAME: string;

  @IsUrl(urlBaseDefiniton)
  STORAGE__URL: string;

  @IsString()
  STORAGE__FOLDER: string = 'shadow';

  @Transform(({ obj }) => obj.STORAGE__SSL === 'true')
  @IsBoolean()
  STORAGE__SSL: boolean = true;

  @IsNumber()
  @IsOptional()
  STORAGE__PORT?: number;

  @IsString()
  STORAGE__USER: string;

  @IsString()
  STORAGE__SECRET: string;

  /**
   *
   * JSON Web Token
   *
   */
  @IsString()
  JWT__TTL: string | number = '7m';

  @IsString()
  @IsOptional()
  JWT__KEY?: string;

  @IsString()
  @IsOptional()
  JWT__PUBLIC?: string;

  @IsString()
  @IsOptional()
  JWT__SECRET?: string;
}
