import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { EnvironmentVariables } from './env.definition';

type EnvType = EnvironmentVariables;
@Injectable()
export class ImageConfigService extends ConfigService<EnvType> {
  override get<K extends keyof EnvType>(propertyPath: K): EnvType[K] {
    return <EnvType[K]>super.get(propertyPath);
  }
}
