import { plainToClass } from 'class-transformer';
import { EnvironmentVariables } from './env.definition';

const testBoolean = (env) => {
  const envs = {
    [env]: 'false',
  };

  do {
    const converted = plainToClass(EnvironmentVariables, envs);
    expect(converted[env]).toBe(envs[env] !== 'false');
    envs[env] = envs[env] === 'false' ? 'true' : 'false';
  } while (envs[env] === 'true');
};

describe('test env defintions file', () => {
  it('check if defaults are set', () => {
    const variables = new EnvironmentVariables();
    expect(variables.HOST).toBeDefined();
    expect(variables.PORT).toBeDefined();
    expect(variables.FORMAT_CONFIG_CLEAN).toBe(false);
    expect(variables.FORMAT_CONFIG_OVERWRITE).toBe(false);
    expect(variables.DATABASE__SSL).toBe(true);
    expect(variables.STORAGE__SSL).toBe(true);
  });

  it('check convertion of FORMAT_CONFIG_OVERWRITE', () => {
    testBoolean('FORMAT_CONFIG_OVERWRITE');
  });

  it('check convertion of FORMAT_CONFIG_CLEAN', () => {
    testBoolean('FORMAT_CONFIG_CLEAN');
  });

  it('check convertion of DATABASE__SSL', () => {
    testBoolean('DATABASE__SSL');
  });
  it('check convertion of STORAGE__SSL', () => {
    testBoolean('STORAGE__SSL');
  });
});
