import { InternalServerErrorException, Module } from '@nestjs/common';
import { S3Module } from '@app/s3';
import { UrlExpression } from '@api/utils/regexp';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { S3StorageService } from './s3.data.service';
import { STORAGE_IDENTIFIER } from './storage.decorator';

@Module({
  imports: [
    S3Module.forRootAsync({
      useFactory: (config: ImageConfigService) => {
        const url = config.get('STORAGE__URL');
        const urlExpression = UrlExpression.setValue(url);
        if (!urlExpression.isValid()) {
          throw new InternalServerErrorException(
            'URL for storage Service is not valid',
          );
        }
        return {
          endPoint: urlExpression.get('host'),
          port: config.get('STORAGE__PORT') || urlExpression.get('port'),
          useSSL: config.get('STORAGE__SSL'),
          accessKey: config.get('STORAGE__USER'),
          secretKey: config.get('STORAGE__SECRET'),
        };
      },
      inject: [ImageConfigService],
    }),
  ],
  providers: [
    {
      provide: STORAGE_IDENTIFIER,
      useClass: S3StorageService,
    },
  ],
  exports: [STORAGE_IDENTIFIER],
})
export class StorageModule {}
