import { Injectable } from '@nestjs/common';
import { StorageInterface } from '@lib/interfaces';
import { StorageDTO } from './dto/storage.dto';

@Injectable()
export abstract class StorageService {
  protected defaultProvider: string;

  protected providerList: string[];

  protected defaultFolder = 'shadow';

  abstract get(url: string): Promise<Buffer>;

  abstract getLink(url: string): Promise<string>;

  abstract getUploadLink(data: StorageInterface | string): Promise<string>;

  abstract getUrl(data: StorageDTO): string;

  abstract save(data: StorageDTO): Promise<string>;

  abstract remove(urls: string[]): Promise<void>;
}
