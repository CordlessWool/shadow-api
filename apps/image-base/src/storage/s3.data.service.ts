import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { Client, ItemBucketMetadata } from 'minio';
import {
  DataServiceInterface,
  FileStorageServiceInterface,
  StorageInterface,
} from '@lib/interfaces';
import { StorageErrorExeption } from '@lib/exeptions/StorageErrorExeption';
import { InjectMinioS3 } from '@app/s3';
import { SelectedRequired } from '@lib/types';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { StorageDTO, StorageOptionsInterface } from './dto/storage.dto';
import { StorageService } from './storage.service';

@Injectable()
export class S3StorageService
  extends StorageService
  implements FileStorageServiceInterface, DataServiceInterface<StorageDTO>
{
  private url: string;

  private folder: string;

  constructor(
    private readonly configService: ImageConfigService,
    @InjectMinioS3() private readonly s3client: Client,
  ) {
    super();
    this.url = this.configService.get('STORAGE__URL');

    this.folder = this.configService.get('STORAGE__FOLDER');
  }

  async onApplicationBootstrap() {
    this.s3client.bucketExists(this.folder, async (errExist, exists) => {
      if (errExist) throw errExist;
      if (!exists) {
        await this.s3client.makeBucket(this.folder, 'eu-central-1');
      }

      await this.s3client.setBucketPolicy(
        this.folder,
        JSON.stringify({
          Version: '2012-10-17',
          Statement: [
            {
              Effect: 'Allow',
              Principal: {
                AWS: ['*'],
              },
              Action: ['s3:GetObject'],
              Resource: ['arn:aws:s3:::shadow/public-*'],
            },
          ],
        }),
      );
    });
  }

  protected generateName(data: StorageInterface): string {
    const { name, uuid, isPrivate } = data;
    const prefix = isPrivate ? 'private' : 'public';
    return `${prefix}-${name.split(' ').join('_')}-${uuid}`;
  }

  getUrl(data: StorageDTO) {
    return this.generateUrl(data);
  }

  generateUrl(data: StorageDTO): string {
    return `${this.url}/${this.folder}/${this.generateName(data)}`;
  }

  // Todo: add to utils
  private escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

  splitUrl(url) {
    const escapedUrl = this.escapeRegExp(this.url);
    // eslint-disable-next-line security/detect-non-literal-regexp
    const regex = new RegExp(`^${escapedUrl}/(.+)/(.+)`);
    const parts = regex.exec(url);
    if (parts?.length !== 3) {
      throw new InternalServerErrorException('url do not match');
    }
    const isPrivate = parts[2].startsWith('private');
    return {
      folder: parts[1],
      name: parts[2],
      isPrivate,
    };
  }

  protected addElementToArray<T>(array: T[], element: T): T[] {
    if (array) {
      array.push(element);
      return array;
    }
    return [element];
  }

  protected reduceUrlsToFolder(urls: string[]): Record<string, Array<string>> {
    return urls.reduce((acc, url) => {
      const { folder, name } = this.splitUrl(url);
      if (folder === this.folder) {
        acc[this.folder] = this.addElementToArray(acc[this.folder], name);
      }

      return acc;
    }, {});
  }

  async getLink(url: string): Promise<string> {
    const { isPrivate, name } = this.splitUrl(url);
    if (isPrivate) {
      return this.s3client.presignedGetObject(this.folder, name, 60);
    }
    return url;
  }

  async getUploadLink(data: string): Promise<string>;
  async getUploadLink(data: StorageInterface): Promise<string>;
  async getUploadLink(data: StorageInterface | string): Promise<string> {
    let name;
    if (typeof data === 'string') {
      name = this.splitUrl(data).name;
    } else {
      name = this.generateName(data);
    }
    return this.s3client.presignedPutObject(this.folder, name, 60000);
  }

  async get(url: string): Promise<Buffer> {
    const { folder, name } = this.splitUrl(url);
    const stream = await this.s3client.getObject(folder, name);
    return new Promise((resolve, reject) => {
      const chunks: Buffer[] = [];
      stream.on('data', (chunk) => chunks.push(chunk));
      stream.once('end', () => {
        const buffer = Buffer.concat(chunks);
        resolve(buffer);
      });
      stream.once('error', (err) => reject(err));
    });
  }

  async updatePermissions(): Promise<void> {
    throw new Error('Not implemented');
  }

  protected mapSaveOptions(
    options: StorageOptionsInterface | undefined,
  ): ItemBucketMetadata {
    const s3Options = {};
    if (options?.mimeType) {
      s3Options['Content-type'] = options.mimeType;
    }
    return s3Options;
  }

  async save(data: SelectedRequired<StorageDTO, 'file'>): Promise<string> {
    try {
      const { file, options } = data;
      const s3Options = this.mapSaveOptions(options);
      await this.s3client.putObject(
        this.folder,
        this.generateName(data),
        file,
        s3Options,
      );
      return this.generateUrl(data);
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  /*
  update(url: string) {
    return 'need to be implement';
  }
  */

  async remove(urls: string[]): Promise<void> {
    try {
      const foldersObject = this.reduceUrlsToFolder(urls);
      await Promise.all(
        Object.entries(foldersObject).map(([folder, element]) =>
          this.s3client.removeObjects(folder, element),
        ),
      );
    } catch (err) {
      console.error(err);
      throw new StorageErrorExeption(err, 'Faild to remove Images');
    }
  }
}
