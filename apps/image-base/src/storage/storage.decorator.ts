import { Inject } from '@nestjs/common';

export const STORAGE_IDENTIFIER = Symbol('STORAGE_INJECTION_IDENTIFIER');

export const InjectStorage = () => Inject(STORAGE_IDENTIFIER);
