import { Image } from '@api/images/entities/image.entity';
import { FormatType } from '@lib/enum';
import * as faker from 'faker';
import { StorageDTO } from './storage.dto';

describe('ImagesService', () => {
  it('Should be createable', () => {
    const name = faker.lorem.word();
    const image = {
      id: faker.datatype.uuid(),
      formatType: 'jpg',
    };
    const dto = StorageDTO.fromImage(<Image>image, name);

    expect(dto.options?.mimeType).toEqual(`image/${image.formatType}`);
    expect(dto.isPrivate).toBe(false);
    expect(dto.format).toBe(FormatType.jpg);
    expect(dto.name).toBe(name);
    expect(dto.uuid).toBe(image.id);
  });
});
