import { Image } from '@api/images/entities/image.entity';
import { attributeType } from '@api/utils/types';
import { FormatType } from '@lib/enum';
import { StorageInterface } from '@lib/interfaces';
import { plainToClass } from 'class-transformer';
import {
  IsBoolean,
  IsEnum,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export interface StorageOptionsInterface {
  mimeType?: string;
  size?: number;
}

export class StorageDTO implements StorageInterface {
  @IsUUID()
  uuid: string;

  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  file?: Buffer;

  @IsEnum(FormatType)
  format: FormatType;

  @IsOptional()
  @IsBoolean()
  isPrivate?: boolean;

  @IsOptional()
  options?: StorageOptionsInterface;

  static fromImage(
    image: Image,
    name: attributeType<StorageDTO, 'name'>,
    file?: Buffer,
    isPrivate = false,
  ) {
    return plainToClass(StorageDTO, {
      uuid: image.id,
      name,
      file,
      format: FormatType[image.formatType],
      isPrivate,
      options: {
        mimeType: `image/${image.formatType}`,
      },
    });
  }
}
