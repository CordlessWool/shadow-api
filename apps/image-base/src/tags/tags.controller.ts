import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  HttpCode,
  Query,
} from '@nestjs/common';
import { ManagementRoles } from '@api/auth/role.decorator';
import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { TagFindOptions } from './dto/find-options.dto';

@Controller('tags')
export class TagsController {
  constructor(private readonly tagsService: TagsService) {}

  /**
   * Creates a tag
   * @param createTagDto
   * @returns
   */
  @ManagementRoles()
  @Post()
  create(@Body() createTagDto: CreateTagDto) {
    return this.tagsService.create(createTagDto);
  }

  /**
   * Return all tags with base Informations
   * @param query
   * @returns
   */
  @Get()
  async findAll(@Query() options: TagFindOptions) {
    return this.tagsService.findAll(options);
  }

  /**
   * Return detailed Informations about a tag with all related imageIds
   * @param id
   * @returns
   */
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.tagsService.findOne(id);
  }

  /**
   * Partial update of a specific tag data change
   * @param id
   * @param updateTagDto
   * @returns
   */
  @ManagementRoles()
  @Put(':id')
  async update(@Param('id') id: string, @Body() updateTagDto: UpdateTagDto) {
    await this.tagsService.update(id, updateTagDto);
  }

  /**
   * Delete a specific tag
   * @param id
   */
  @ManagementRoles()
  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id') id: string) {
    await this.tagsService.remove(id);
  }
}
