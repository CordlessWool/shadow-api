/* eslint-disable @typescript-eslint/no-inferrable-types */
import { DefaultSizes } from '@lib/enum';
import { IsHexRgb } from '@lib/utils/validation/IsHexRgb';
import {
  IsJSON,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class CreateTagDto {
  @IsString()
  @MinLength(1)
  @MaxLength(DefaultSizes.NAME)
  name: string;

  /**
   * Short description for the tag
   */
  @IsOptional()
  @IsString()
  @MaxLength(DefaultSizes.DESCRIPTION)
  description?: string;

  /**
   * Long story to the tag
   */
  @IsOptional()
  @IsString()
  story?: string;

  /**
   * Color code of the tag
   */
  @IsHexRgb()
  color: string = '#81238E';

  /**
   * List of additinal attributes
   */
  @IsOptional()
  @IsJSON()
  attributes?: Record<string, string | number | boolean>;
}
