import { RequestQueryOptions } from '@lib/utils/dto/Request.options';
import { IsBoolean, IsOptional, IsString, MaxLength } from 'class-validator';
import { DefaultSizes } from '@lib/enum';

export class TagFindOptions extends RequestQueryOptions {
  /**
   * List of tags should be return
   */
  @IsOptional()
  @IsString({ each: true })
  @MaxLength(DefaultSizes.NAME)
  name?: string | Array<string>;

  /**
   * Search string to find matching tags
   */
  @IsOptional()
  @IsString()
  search?: string;

  /**
   * Set to true to get all Informations about a tag
   */
  @IsOptional()
  @IsBoolean()
  full?: boolean;
}
