import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository, UpdateResult } from 'typeorm';
import { FindOptionsBuilder } from '@api/utils/FindOptionsBuilder';
import { tagId } from '../types/tag';
import { CreateTagDto } from './dto/create-tag.dto';
import { UpdateTagDto } from './dto/update-tag.dto';
import { Tag } from './entities/tag.entity';
import { TagFindOptions } from './dto/find-options.dto';

@Injectable()
export class TagsService {
  constructor(
    @InjectRepository(Tag) private readonly tagReposotory: Repository<Tag>,
  ) {}

  async create(createTagDto: CreateTagDto) {
    const tag = new Tag();
    tag.name = createTagDto.name;
    if (createTagDto.description) tag.description = createTagDto.description;
    if (createTagDto.color) tag.color = createTagDto.color;
    const saved = await this.tagReposotory.save(tag);
    return saved.id;
  }

  async findAll(options: TagFindOptions = {}) {
    const { full, search, ...elements } = options;

    const findManyOptions = new FindOptionsBuilder<Tag>({
      select: ['id', 'name', 'description', 'color', 'attributes'],
    });

    findManyOptions
      .setCondition(full, true)
      .extendSelect(['permission', 'createdAt', 'updatedAt'])
      .setWhere(elements)
      .setCondition(search, true)
      .extendWhere({
        name: Like(`%${search}%`),
      });

    const [result, total] = await this.tagReposotory.findAndCount(
      findManyOptions.get(),
    );
    return {
      data: result,
      total,
    };
  }

  async findOne(id: tagId) {
    const result = await this.tagReposotory.findOne(id, {
      select: [
        'name',
        'description',
        'color',
        'attributes',
        'id',
        'permission',
      ],
    });
    if (!result) {
      throw new NotFoundException();
    }
    return result;
  }

  async update(id: tagId, updateTagDto: UpdateTagDto) {
    const tags: UpdateResult = await this.tagReposotory.update(
      id,
      updateTagDto,
    );
    if (tags.affected === 0) {
      throw new NotFoundException('Record was not found');
    }
    return id;
  }

  async remove(id: tagId) {
    const tag = await this.tagReposotory.findOne(id);
    if (!tag) throw new BadRequestException();
    const [removedTag] = await this.tagReposotory.remove([tag]);
    if (!removedTag) throw new InternalServerErrorException();
  }
}
