import { imageId } from '@api//types/image';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  RelationId,
  UpdateDateColumn,
} from 'typeorm';
import { EntityBase } from '@api//Objects/EnitityBase';
import { Meta } from '@api//images/entities/meta.entity';
import { DefaultSizes } from '@lib/enum';

@Entity()
export class Tag extends EntityBase {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true,
    type: 'varchar',
    length: DefaultSizes.NAME,
  })
  name: string;

  @Column({
    default: '',
    type: 'varchar',
    length: DefaultSizes.DESCRIPTION,
  })
  description: string;

  @Column({
    nullable: true,
    type: 'text',
  })
  story?: string;

  @Column({
    nullable: true,
    type: 'varchar',
    length: 7,
  })
  color?: string;

  @Column({
    default: 'public',
  })
  permission: string;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  attributes: Record<string, unknown>;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToMany(() => Meta, (meta) => meta.tags)
  images: Meta[];

  @RelationId((tag: Tag) => tag.images)
  imageIds: imageId[];
}
