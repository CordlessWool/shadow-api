import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import * as faker from 'faker';
import { NotFoundException } from '@nestjs/common';
import { Tag } from './entities/tag.entity';
import { TagsService } from './tags.service';
import { CreateTagDto } from './dto/create-tag.dto';

const mockReposotory = {
  save: jest.fn(),
  find: jest.fn(),
  findAndCount: jest.fn(),
  findOne: jest.fn(),
  update: jest.fn(),
  removeById: jest.fn(),
  remove: jest.fn(),
};

const createTestData = (amount = 10): Array<Tag> => {
  const data: Tag[] = [];

  for (let i = 0; i < amount; i += 1) {
    const tag = new Tag();
    tag.id = faker.datatype.uuid();
    tag.name = faker.hacker.noun();
    data.push(tag);
  }

  return data;
};

describe('TagsService', () => {
  let service: TagsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TagsService,
        {
          provide: getRepositoryToken(Tag),
          useValue: mockReposotory,
        },
      ],
    }).compile();

    service = module.get<TagsService>(TagsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    let tags: Array<Tag>;
    let spy;

    beforeAll(() => {
      tags = createTestData(15);

      spy = jest
        .spyOn(mockReposotory, 'findOne')
        .mockImplementation((id) => tags.find((tag) => tag.id === id));
    });

    it('should call findOne of Reposotory', async () => {
      await service.findOne(tags[0].id);
      expect(spy).toHaveBeenCalled();
    });

    it('throw NotFound error', async () => {
      try {
        await service.findOne(faker.datatype.uuid());
      } catch (err) {
        expect(err).toBeInstanceOf(NotFoundException);
        expect(err.getStatus()).toBe(404);
      }
    });

    it('should return one tag by id', async () => {
      const positionOfArray = 7;

      const { id } = tags[positionOfArray];
      const tag = await service.findOne(id);

      expect(tag).toBe(tags[positionOfArray]);
    });
  });

  describe('findAll', () => {
    let database: Array<Tag>;
    const amountOfTags = 27;
    let spy;

    beforeAll(() => {
      database = createTestData(amountOfTags);

      spy = jest
        .spyOn(mockReposotory, 'findAndCount')
        .mockImplementation(() => [database, amountOfTags]);
    });

    it('should call findAndCount of Reposotory', async () => {
      await service.findAll();
      expect(spy).toHaveBeenCalled();
    });

    it('should return all tags', async () => {
      const tags = await service.findAll();

      expect(tags.data.length).toBe(amountOfTags);
      expect(tags.total).toBe(amountOfTags);
      expect(tags.data).toBe(database);
    });
  });

  describe('create', () => {
    let testId;
    let spy;
    let dto;

    beforeAll(() => {
      spy = jest
        .spyOn(mockReposotory, 'save')
        .mockImplementation((tag: Tag) => {
          return {
            ...tag,
            id: testId,
          };
        });
    });

    beforeEach(() => {
      dto = new CreateTagDto();
      dto.name = faker.hacker.noun();
      testId = faker.datatype.uuid();
    });

    it('should call save of Reposotory', async () => {
      await service.create(dto);
      expect(spy).toBeCalled();
    });

    it('return only id', async () => {
      const tagId = await service.create(dto);
      expect(tagId).toBe(testId);
    });
  });

  describe('update', () => {
    let spy;
    let database;
    const amountOfTags = 80;
    beforeAll(() => {
      spy = jest
        .spyOn(mockReposotory, 'update')
        .mockImplementation((id, data) => {
          const i = database.findIndex((d) => d.id === id);
          if (i >= 0) {
            database[i] = data;
            return {
              raw: [database[i]],
              affected: 1,
            };
          }

          return {
            raw: [],
            affected: 0,
          };
        });
    });

    beforeEach(() => {
      database = createTestData(amountOfTags);
    });

    it('should call update of Reposotory', async () => {
      const someTag = database[0];
      await service.update(someTag.id, {});
      expect(spy).toBeCalled();
    });

    it('should only return id', async () => {
      const someTag = database[7];
      const changedTag = {
        ...someTag,
        desciption: faker.lorem.sentences(),
      };
      const tagId = await service.update(someTag.id, changedTag);
      expect(tagId).toBe(someTag.id);
    });

    /** TODO: Write test about fail */
  });

  describe('remove', () => {
    let spy;

    beforeAll(() => {
      spy = jest
        .spyOn(mockReposotory, 'remove')
        .mockImplementation((tags: Tag[]) => tags);

      jest
        .spyOn(mockReposotory, 'findOne')
        .mockImplementation((id) => ({ id }));
    });

    it('should call remove of Reposotory', async () => {
      try {
        const id = faker.datatype.uuid();
        await service.remove(id);
        expect(spy).toBeCalled();
      } catch (err) {
        expect(err).toBeUndefined();
      }
    });

    it('should return notthing', async () => {
      try {
        const id = faker.datatype.uuid();
        const deleted = await service.remove(id);
        expect(deleted).toBeUndefined();
      } catch (err) {
        expect(err).toBeUndefined();
      }
    });
  });
});
