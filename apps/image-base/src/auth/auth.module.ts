import { ImageConfigService } from '@api/image-config/image-config.service';
import { RequireExeption } from '@lib/exeptions/RequiredExeption';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (config: ImageConfigService) => {
        const ttl = config.get('JWT__TTL') || '7m';
        let secretOrKey = config.get('JWT__KEY');
        if (!secretOrKey) {
          secretOrKey = config.get('JWT__SECRET');
        }

        if (!secretOrKey) {
          throw new RequireExeption('secret or key');
        }
        return {
          secretOrPrivateKey: secretOrKey,
          signOptions: { expiresIn: ttl },
        };
      },
      inject: [ImageConfigService],
    }),
  ],
  controllers: [],
  providers: [JwtStrategy],
})
export class AuthModule {}
