import { Role } from '@lib/enum';
import { SetMetadata } from '@nestjs/common';
import { ROLES_KEY } from './static';

export const Roles = (...roles: Role[]) => SetMetadata(ROLES_KEY, roles);
export const PhotoRoles = () =>
  Roles(Role.admin, Role.editor, Role.photographer);
export const ManagementRoles = () => Roles(Role.admin, Role.editor);
