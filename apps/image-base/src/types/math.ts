export type CoordinatesTuple = [number, number];
export type CoordinatesObject = { lat: number; lon: number };
