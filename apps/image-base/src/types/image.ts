export type imageId = string;

export interface MetaBaseWithLinks {
  id: imageId;
  name: string;
  imageUrl: string;
  dataUrl: string;
}
