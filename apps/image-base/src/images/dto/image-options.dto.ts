import { FormatFit, FormatType } from '@lib/enum';
import { attributeType } from '@api/utils/types';
import {
  IsEnum,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { Type } from 'class-transformer';
import { Image } from '../entities/image.entity';

export class ImageOptionsDto {
  @IsEnum(FormatFit)
  @IsOptional()
  fit?: FormatFit;

  @Type(() => Number)
  @IsNumber()
  @IsPositive()
  @IsOptional()
  height?: number;

  @Type(() => Number)
  @IsNumber()
  @IsPositive()
  @IsOptional()
  width?: number;

  @IsEnum(FormatType)
  @IsOptional()
  type?: FormatType;

  @IsString()
  @IsOptional()
  ref?: attributeType<Image, 'formatRefName'>;
}
