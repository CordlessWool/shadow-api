import { IsNumber, IsOptional } from 'class-validator';

export class FindOptionsDto {
  @IsOptional()
  @IsNumber()
  height?: number;

  @IsOptional()
  @IsNumber()
  width?: number;
}
