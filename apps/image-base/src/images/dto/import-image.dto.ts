import { IsNumber, IsString } from 'class-validator';

export class ImportImageDto {
  /** Field name specified in the form */
  @IsString()
  fieldname: string;

  /** Name of the file on the user's computer */
  @IsString()
  originalname: string;

  /** Encoding type of the file */
  @IsString()
  encoding: string;

  /** Mime type of the file */
  @IsString()
  mimetype: string;

  /** Size of the file in bytes */
  @IsNumber()
  size: number;

  /** The folder to which the file has been saved (DiskStorage) */
  @IsString()
  destination: string;

  /** The name of the file within the destination (DiskStorage) */
  @IsString()
  filename: string;

  /** Location of the uploaded file (DiskStorage) */
  @IsString()
  path: string;

  /** A Buffer of the entire file (MemoryStorage) */
  @IsString()
  buffer: Buffer;
}
