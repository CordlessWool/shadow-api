import { imageId } from '@api/types/image';
import { IsString, IsUUID } from 'class-validator';
import { ImportImageDto } from './import-image.dto';

export class CreateMetaDto {
  @IsUUID()
  uuid: string;

  @IsString()
  name: string;

  @IsString()
  file: Buffer;

  static fromImportImageDto(id: imageId, dto: ImportImageDto) {
    const metaDto = new CreateMetaDto();
    metaDto.uuid = id;
    metaDto.name = dto.fieldname;
    metaDto.file = dto.buffer;

    return metaDto;
  }
}
