import { tagId } from '@api/types/tag';
import { DefaultSizes } from '@lib/enum';
import {
  IsLatitude,
  IsLongitude,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
} from 'class-validator';

export class UpdateMetaDto {
  @IsOptional()
  @IsString()
  @MaxLength(DefaultSizes.NAME)
  name: string;

  @IsOptional()
  @IsString()
  @MaxLength(DefaultSizes.DESCRIPTION)
  description?: string;

  @IsOptional()
  @IsUUID(4, { each: true })
  tags?: Array<tagId>;

  @IsOptional()
  @IsLatitude()
  lat?: number;

  @IsOptional()
  @IsLongitude()
  lon?: number;
}
