import { ImageManipulationInterface } from '@api/formats/types';
import { IsObject, IsString, IsUrl, IsUUID } from 'class-validator';

export class CreateImageDto {
  @IsUUID()
  uuid: string;

  @IsString()
  name: string;

  @IsUrl()
  url: string;

  @IsObject()
  manipulation: ImageManipulationInterface;
}
