import { IsEnum, IsIn } from 'class-validator';
import { ImageStatus } from '../defines/image-status.enum';
import { Image } from '../entities/image.entity';
import { formatDefaultSelectAttributes } from '../images.defaults';

export class FormatOptionsDTO {
  @IsIn([...formatDefaultSelectAttributes], {
    each: true,
  })
  attributes: (keyof Image)[] = formatDefaultSelectAttributes;

  @IsEnum(ImageStatus, { each: true })
  status: ImageStatus[] = [ImageStatus.complete];
}
