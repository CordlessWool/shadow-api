import { imageId } from '@api/types/image';
import { EntityRepository, Repository } from 'typeorm';
import { Meta } from './entities/meta.entity';

@EntityRepository(Meta)
export class MetaRepository extends Repository<Meta> {
  async removeById(id: imageId) {
    const meta = await this.findOne(id);
    if (meta) {
      await this.remove(meta);
      return true;
    }

    return false;
  }
}
