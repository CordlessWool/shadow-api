import { imageId } from '@api/types/image';
import { FindOptionsBuilder } from '@api/utils/FindOptionsBuilder';
import { UuidExpression } from '@api/utils/regexp';
import { EntityRepository, Repository } from 'typeorm';
import { ImageStatus } from './defines/image-status.enum';
import { Image } from './entities/image.entity';
import { MetaRepository } from './meta.reposotory';

export interface ClosestOption {
  height?: number;
  width?: number;
}

@EntityRepository(Image)
export class ImageReposotory extends Repository<Image> {
  findAllWithIdAndCount(
    id: imageId,
    options = new FindOptionsBuilder<Image>(),
  ): Promise<[Image[], number]> {
    options.extendWhere({
      metaId: id,
    });
    return this.findAndCount(options.get());
  }

  findAllWithId(
    id: imageId,
    options = new FindOptionsBuilder<Image>(),
  ): Promise<Image[]> {
    options.extendWhere({
      metaId: id,
    });
    return this.find(options.get());
  }

  /**
   * Find the closest image from heigth and/or width
   * If values are not of a valid type the functions throws an error
   *
   * @param id
   * @param options
   * @returns
   */
  async findClosest(id: imageId, options: ClosestOption): Promise<Image[]> {
    if (!UuidExpression.setValue(id).isValid()) {
      throw new Error('ID have to be a uuid');
    }

    let orderQuery = '';
    for (const [key, value] of Object.entries(options)) {
      if (value) {
        if (Number.isNaN(value)) throw new Error(`${key} have to be a number`);
        orderQuery += ` ABS(${key} - ${value}) ASC`;
      }
    }

    if (orderQuery.length === 0) {
      throw new Error('At least one size have to be set');
    }

    // orderQuery has a whitespace at beginning
    return this.query(
      `select * from image where "metaId" = ${id} order by${orderQuery}`,
    );
  }

  async setStatus(id: imageId, status: ImageStatus) {
    await this.update(id, { status });
  }

  /**
   * remove all inforamtation relating to an image and all it verisons
   * @param id
   */
  async removeAll(id: imageId): Promise<{ images: number; metas: number }> {
    const images = await this.findAllWithId(id);
    let deletedMeta = false;
    await this.manager.transaction(async (transManager) => {
      const tImageRepo = transManager.getCustomRepository(ImageReposotory);
      const tMetaRepo = transManager.getCustomRepository(MetaRepository);

      await Promise.all(images.map((image) => tImageRepo.remove(image)));
      deletedMeta = await tMetaRepo.removeById(id);
    });

    return {
      images: images.length,
      metas: deletedMeta ? 1 : 0,
    };
  }
}
