import { Test, TestingModule } from '@nestjs/testing';
import { FormatsService } from '@api/formats/formats.service';
import { STORAGE_IDENTIFIER } from '@api/storage/storage.decorator';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { ConfigService } from '@nestjs/config';
import { ImageReposotory } from './image.repository';
import { ImagesService } from './images.service';
import { MetaService } from './meta.service';

describe('ImagesService', () => {
  let service: ImagesService;

  beforeEach(async () => {
    process.env.SERVICE_REVAMP = 'https://test.de';

    const module: TestingModule = await Test.createTestingModule({
      imports: [],
      providers: [
        ImagesService,
        {
          provide: ImageConfigService,
          useClass: ConfigService,
        },
        {
          provide: STORAGE_IDENTIFIER,
          useValue: {},
        },
        {
          provide: MetaService,
          useValue: {},
        },
        {
          provide: ImageReposotory,
          useValue: {},
        },
        {
          provide: FormatsService,
          useValue: {},
        },
        {
          provide: 'revamp/image',
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ImagesService>(ImagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
