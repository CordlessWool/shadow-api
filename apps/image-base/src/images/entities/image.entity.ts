import * as sharp from 'sharp';
import { formatId, ImageManipulationInterface } from '@api/formats/types';
import { EntityBase } from '@api/Objects/EnitityBase';
import { imageId } from '@api/types/image';
import { ObjectExtractor } from '@api/utils/ObjectExtractor';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  Index,
  JoinColumn,
  RelationId,
  CreateDateColumn,
} from 'typeorm';
import { v4 as uuidv4 } from 'uuid';

import { OnlyAttributes, attributeType } from '@api/utils/types';
import { AttributeValueExeption } from '@lib/exeptions/MissingAttributeExeption';
import { FormatFit, FormatType } from '@lib/enum';
import { MSUploadReturnDto } from '@revamp/dto/ms-upload.dto';
import { Meta } from './meta.entity';
import { ImageMetaInterface } from '../defines/image-meta.interface';
import { ImageFormatFit } from '../defines/image-format-fit.enum';
import { ImageStatus } from '../defines/image-status.enum';

const getMetadata = async (image: Buffer): Promise<ImageMetaInterface> => {
  const meta = await sharp(image).metadata();
  const extractor = new ObjectExtractor(['height', 'width', 'size', 'format']);
  return extractor.reduce(meta);
};

@Entity()
export class Image extends EntityBase {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index({ unique: true })
  @Column()
  url: string;

  @ManyToOne(() => Meta, (meta: Meta) => meta.images, {
    nullable: false,
  })
  @JoinColumn({ name: 'metaId', referencedColumnName: 'id' })
  meta: Meta;

  @RelationId((image: Image) => image.meta)
  @Column()
  metaId: imageId;

  @CreateDateColumn()
  createdAt: Date;

  @Column({
    type: 'varchar',
    length: 5,
  })
  formatType: keyof typeof FormatType;

  protected _formatFit: ImageFormatFit;

  set formatFit(value: FormatFit | ImageFormatFit) {
    switch (value) {
      case ImageFormatFit.cutted:
      case FormatFit.contain:
      case FormatFit.cover:
        this._formatFit = ImageFormatFit.cutted;
        break;
      case ImageFormatFit.stretched:
      case FormatFit.fill:
        this._formatFit = ImageFormatFit.stretched;
        break;
      case ImageFormatFit.original:
      case FormatFit.inside:
      case FormatFit.outside:
      default:
        this._formatFit = ImageFormatFit.original;
        break;
    }
  }

  @Column({
    name: 'formatFit',
    type: 'varchar',
    length: 20,
    nullable: true,
  })
  get formatFit() {
    return this._formatFit;
  }

  @Column({
    type: 'smallint',
    nullable: true,
  })
  height: number | null;

  @Column({
    type: 'smallint',
    nullable: true,
  })
  width: number | null;

  @Column({
    type: 'integer',
    nullable: true,
  })
  size: number | null;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  formatRefName: formatId | 'original';

  // TODO: replace string with enum and define types
  @Column({
    type: 'varchar',
    length: 10,
  })
  status: keyof typeof ImageStatus;

  static async fromOriginalFile(file: Buffer): Promise<Image> {
    const meta = await getMetadata(file);
    const id = uuidv4();

    return Image.from({
      ...meta,
      formatType: meta.format,
      metaId: id,
      formatFit: ImageFormatFit.original,
      formatRefName: 'original',
      id,
    });
  }

  static async fromFile(
    file: Buffer,
    additinals: {
      metaId: imageId;
      formatFit: attributeType<Image, 'formatFit'>;
      formatRefName: attributeType<Image, 'formatRefName'>;
    },
  ): Promise<Image> {
    const meta = await getMetadata(file);

    return Image.from({
      ...meta,
      formatType: meta.format,
      ...additinals,
      id: uuidv4(),
    });
  }

  private static from(
    data: Omit<
      OnlyAttributes<Image>,
      'url' | 'createdAt' | 'meta' | 'status' | '_formatFit'
    >,
  ) {
    const image = new Image();

    image.id = data.id;
    image.metaId = data.metaId;
    image.formatType = data.formatType;
    image.height = data.height;
    image.width = data.width;
    image.size = data.size;
    image.formatFit = data.formatFit;
    image.formatRefName = data.formatRefName;
    image.status = ImageStatus.complete;

    return image;
  }

  static fromFormat(data: ImageManipulationInterface, original: Image): Image {
    const image = new Image();

    const {
      height = null,
      width = null,
      fit = ImageFormatFit.original,
      type = null,
      id,
    } = data;

    image.id = uuidv4();
    image.metaId = original.metaId;
    image.height = height;
    image.width = width;
    image.formatFit = fit;
    image.formatType = type || original.formatType;
    image.formatRefName = id;
    image.status = ImageStatus.creating;

    return image;
  }

  updateFromMeta(data: Omit<MSUploadReturnDto, 'id'>): void {
    // TODO: check if data has all needed properties

    this.height = data.height;
    this.width = data.width;
    this.size = data.size;
    this.formatFit = data.fit;
    this.status = ImageStatus.complete;
  }

  calcRelativeHeight(height: number | undefined | null): number;
  calcRelativeHeight(height: number | undefined | null, image: Image): number;
  // eslint-disable-next-line default-param-last
  calcRelativeHeight(height = 0, image?: Image): number {
    if (!this.height) throw new AttributeValueExeption(this, 'height');
    if (image) {
      return this.calcRelativeHeight(height) - image.calcRelativeHeight(height);
    }
    return Math.abs(this.height - height);
  }

  calcRelativeWidth(width: number | undefined | null): number;
  calcRelativeWidth(width: number | undefined | null, image: Image): number;
  // eslint-disable-next-line default-param-last
  calcRelativeWidth(width = 0, image?: Image): number {
    if (!this.width) throw new AttributeValueExeption(this, 'width');
    if (image) {
      return this.calcRelativeWidth(width) - image.calcRelativeWidth(width);
    }
    return Math.abs(this.width - width);
  }

  static sortByAbsoluteFactor(
    images: Image[],
    factor: Partial<Pick<Image, 'height' | 'width'>>,
  ): Image[] {
    const { height, width } = factor;
    return images.sort((a, b) => {
      const calcHeight = height ? a.calcRelativeHeight(height, b) : 0;
      const calcWidth = width ? a.calcRelativeWidth(width, b) : 0;

      return (calcHeight + calcWidth) / 2;
    });
  }
}
