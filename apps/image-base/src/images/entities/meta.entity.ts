import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
  RelationId,
  JoinTable,
  ValueTransformer,
  UpdateDateColumn,
  CreateDateColumn,
} from 'typeorm';
import exifr from 'exifr';
import { DefaultSizes } from '@lib/enum';
import { EntityBase } from '../../Objects/EnitityBase';
import { Tag } from '../../tags/entities/tag.entity';
import { Image } from './image.entity';
import { tagId } from '../../types/tag';
import { CreateMetaDto } from '../dto/create-meta.dto';

export interface LatLonPoint {
  lat: number;
  lon: number;
}

export class PointTransformer implements ValueTransformer {
  to(value?: LatLonPoint): string | undefined {
    if (value) {
      const { lat, lon } = value;
      return `${lat},${lon}`;
    }
    return undefined;
  }

  from(value?: Record<'x' | 'y', number>): LatLonPoint | undefined {
    if (value) {
      const { x: lat, y: lon } = value;
      return { lat, lon };
    }
    return undefined;
  }
}

@Entity()
export class Meta extends EntityBase {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'varchar',
    length: DefaultSizes.NAME,
  })
  name: string;

  @Column({
    type: 'point',
    nullable: true,
    transformer: new PointTransformer(),
  })
  gps: LatLonPoint;

  @CreateDateColumn({ default: new Date() })
  createdAt: Date;

  @UpdateDateColumn({ default: new Date() })
  uploadedAt: Date;

  @Column({
    nullable: true,
  })
  updatedAt: Date;

  @ManyToMany(() => Tag, (tag) => tag.images)
  @JoinTable()
  tags: Tag[];

  @RelationId((meta: Meta) => meta.tags)
  tagIds: tagId[];

  @Column({
    nullable: true,
    type: 'varchar',
    length: DefaultSizes.DESCRIPTION,
  })
  description: string;

  @OneToMany(() => Image, (image) => image.meta)
  images: Image[];

  public getUrls(): Array<string> {
    return this.images.map((res) => {
      return res.url;
    });
  }

  static async fromMetaDTO(dto: CreateMetaDto): Promise<Meta> {
    const { latitude, longitude, CreateDate, DateTimeOriginal } =
      (await exifr.parse(dto.file)) || {};
    const meta = new Meta();
    meta.id = dto.uuid;
    meta.name = dto.name;
    meta.createdAt = DateTimeOriginal || CreateDate || new Date();
    if (latitude != null && longitude != null)
      meta.gps = { lat: latitude, lon: longitude };

    return meta;
  }
}
