import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StorageModule } from '@api/storage/storage.module';
import { FormatsModule } from '@api/formats/formats.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { ImagesService } from './images.service';
import { ImagesController } from './images.controller';
import { Image } from './entities/image.entity';
import { ImageReposotory } from './image.repository';
import { Meta } from './entities/meta.entity';
import { MetaRepository } from './meta.reposotory';
import { MetaService } from './meta.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Image, ImageReposotory, Meta, MetaRepository]),
    StorageModule,
    FormatsModule,
    ClientsModule.registerAsync([
      {
        name: 'revamp/image',
        imports: [ImageConfigService],
        useFactory: (configService: ImageConfigService) => {
          const url = configService.get('SERVICE__NATS');
          return {
            transport: Transport.NATS,
            options: {
              servers: [url],
              queue: 'image_revamp',
            },
          };
        },
        inject: [ImageConfigService],
      },
    ]),
  ],
  controllers: [ImagesController],
  providers: [ImagesService, MetaService],
})
export class ImagesModule {}
