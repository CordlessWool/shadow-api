import { BadRequestException, Injectable } from '@nestjs/common';

import { imageId, MetaBaseWithLinks } from '@api/types/image';
import { DataResponse } from '@lib/types';
import { Meta } from './entities/meta.entity';
import { UpdateMetaDto } from './dto/update-meta.dto';

import { CreateMetaDto } from './dto/create-meta.dto';
import { MetaRepository } from './meta.reposotory';

@Injectable()
export class MetaService {
  constructor(private readonly metaRepo: MetaRepository) {}

  async create(imageDto: CreateMetaDto) {
    const image = await Meta.fromMetaDTO(imageDto);

    const saved = await this.metaRepo.save(image);
    return saved.id;
  }

  async findAll(offset = 0, take = 100): DataResponse<MetaBaseWithLinks> {
    const [data, total] = await this.metaRepo.findAndCount({
      select: ['id', 'name'],
      skip: offset,
      take,
    });

    const modifiedData = data.map((image) => ({
      ...image,
      imageUrl: `http://localhost:3000/images/${image.id}`,
      dataUrl: `http://localhost:3000/images/${image.id}/meta`,
    }));

    return {
      data: modifiedData,
      offset,
      taken: modifiedData.length,
      total,
    };
  }

  async findOne(id: imageId): Promise<Meta> {
    const data = await this.metaRepo.findOne(id);
    if (data == null) {
      throw new BadRequestException();
    } else {
      return data;
    }
  }

  async patch(id: imageId, dto: UpdateMetaDto) {
    const { lat, lon, ...other } = dto;
    const patchData: Partial<Omit<Meta, 'tags' | 'images'>> = {
      ...other,
    };
    if (lat && lon) patchData.gps = { lat, lon };
    await this.metaRepo.update(id, patchData);
  }

  remove() {
    throw new BadRequestException(`Please use image route to remove a image`);
  }
}
