import {
  BadRequestException,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Processor } from '@nestjs/bull';
import { StorageService } from '@api/storage/storage.service';
import { StorageDTO } from '@api/storage/dto/storage.dto';
import { imageId } from '@api/types/image';
import {
  FindOptionsBuilder,
  FindOptionsBuilderExeption,
  FindOptionsBuilderExpectionTypes,
} from '@api/utils/FindOptionsBuilder';
import { FormatsService } from '@api/formats/formats.service';
import { ClientProxy } from '@nestjs/microservices';
import { MessageStatus, MessageType, MSUploadReturnType } from '@revamp/types';
import { MSUploadDto } from '@revamp/dto/ms-upload.dto';
import { ImageConfigService } from '@api/image-config/image-config.service';
import { InjectStorage } from '@api/storage/storage.decorator';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { DataResponse } from '@lib/types';
import { CreateImageDto } from './dto/create-image.dto';
import { Image } from './entities/image.entity';
import { ImportImageDto } from './dto/import-image.dto';
import { CreateMetaDto } from './dto/create-meta.dto';
import { FindOptionsDto } from './dto/find-options.dto';
import { ImageOptionsDto } from './dto/image-options.dto';
import { ImageReposotory } from './image.repository';
import { FormatOptionsDTO } from './dto/format-options.dto';
import { MetaService } from './meta.service';
import { ImageFormatFit } from './defines/image-format-fit.enum';
import { ImageStatus } from './defines/image-status.enum';
import { formatDefaultSelectAttributes } from './images.defaults';

@Processor('manipulation')
@Injectable()
export class ImagesService {
  revampUrl: string;

  constructor(
    @InjectStorage() private readonly storageService: StorageService,
    private readonly imageRepo: ImageReposotory,
    private readonly metaService: MetaService,
    private readonly configService: ImageConfigService,
    private readonly formatService: FormatsService,
    @Inject('revamp/image') private readonly revampClient: ClientProxy,
  ) {
    this.revampUrl = this.configService.get('SERVICE__REVAMP');
  }

  async import(imageDto: ImportImageDto): Promise<string> {
    const { fieldname: name, buffer: file } = imageDto;

    const image = await Image.fromOriginalFile(file);
    const storageDto = StorageDTO.fromImage(image, name, file, true);

    image.url = this.storageService.getUrl(storageDto);

    const metaDto = CreateMetaDto.fromImportImageDto(image.id, imageDto);
    await this.metaService.create(metaDto);
    await this.save(image, storageDto);

    await this.createVersion(file, image, name);
    return image.id;
  }

  private async createVersion(file: Buffer, image: Image, name): Promise<void> {
    const formats = await this.formatService.bufferedFind();
    const preparedManipulations = await Promise.all(
      formats.map(async (format) => {
        const manipulatedImage = Image.fromFormat(format, image);
        const mStorageDto = StorageDTO.fromImage(manipulatedImage, name);
        const url = this.storageService.getUrl(mStorageDto);
        manipulatedImage.url = url;
        const singedUrl = await this.storageService.getUploadLink(mStorageDto);
        await this.imageRepo.save(manipulatedImage);
        return {
          ...format,
          id: manipulatedImage.id,
          url: singedUrl,
        };
      }),
    );

    this.revampClient
      .send<MSUploadReturnType, MSUploadDto>(
        { cmd: 'transform', upload: true, multi: true },
        {
          image: file.toString('base64url'),
          formats: preparedManipulations,
        },
      )
      .subscribe((res) => this.updateStatusByRevamp(res));
  }

  private async updateStatusByRevamp(message: MSUploadReturnType) {
    if (message.data != null) {
      const { id } = message.data;
      if (message.status === MessageStatus.ok) {
        const { data } = message;
        const updateData: QueryDeepPartialEntity<Image> = {
          height: data.height,
          width: data.width,
          size: data.size,
          formatFit: data.fit,
          status: ImageStatus.complete,
        };
        this.imageRepo.update(id, updateData);
      } else if (message.status === MessageStatus.info) {
        if (message.type === MessageType.not_changed) {
          await this.imageRepo.delete(id);
        }
      } else if (message.status === MessageStatus.error) {
        this.imageRepo.setStatus(id, ImageStatus.error);
      }
    }
  }

  /**
   * Return all Formats existing from this image
   *
   * @param id
   * @param options
   * @returns
   */
  async getFormats(
    id: imageId,
    options?: FormatOptionsDTO,
  ): Promise<DataResponse<Image>> {
    try {
      const selectedAttributes: (keyof Image)[] =
        options?.attributes || formatDefaultSelectAttributes;
      const optionBuilder = new FindOptionsBuilder<Image>();
      optionBuilder
        .setSelectResctriction(formatDefaultSelectAttributes)
        .setSelect(selectedAttributes);
      const [data, total] = await this.imageRepo.findAllWithIdAndCount(
        id,
        optionBuilder,
      );
      return {
        data,
        total,
      };
    } catch (err) {
      if (err instanceof FindOptionsBuilderExeption) {
        switch (err.type) {
          case FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION:
            throw new BadRequestException(
              `Only allows: ${formatDefaultSelectAttributes.join(' ')}`,
            );
        }
      }

      throw new InternalServerErrorException(err);
    }
  }

  protected loadImage(imageDto: CreateImageDto): Promise<Buffer> {
    const { url } = imageDto;
    return this.storageService.get(url);
  }

  private async save(image: Image, storageDto: StorageDTO) {
    try {
      await this.imageRepo.save(image);
      await this.storageService.save(storageDto);
    } catch (err) {
      // TODO: roleback
      console.error(err);
      throw err;
    }
  }

  async findAll(_options: FindOptionsDto) {
    const { height, width } = _options;
    const optionBuilder = new FindOptionsBuilder<Image>();
    const calculate = (n: number) => n * 0.4;
    // TODO: add information about permission (need user permission service)
    optionBuilder
      .between('height', height, calculate)
      .between('width', width, calculate)
      .onetimeCondition(!height || !width)
      .extendWhere({
        formatFit: 'original',
      });
    // TODO: think about what happen if original is restricted
    const images = await this.imageRepo.find(optionBuilder.get());
    return images;
  }

  async findByRef(id: imageId, ref: string) {
    let image: Image | undefined;
    if (ref === 'original') {
      // TODO: check for permissions
      image = await this.imageRepo.findOne(id);
    } else {
      [image] = await this.imageRepo.find({
        id,
        formatRefName: ref,
        status: ImageStatus.complete,
      });
    }

    if (!image) throw new NotFoundException();
    return {
      url: await this.storageService.getLink(image.url),
      statusCode: 303,
    };
  }

  async findOne(id: imageId, options: ImageOptionsDto) {
    const { ref, ...manipulations } = options;
    const {
      height,
      width,
      type,
      fit = ImageFormatFit.original,
    } = manipulations;
    try {
      if (ref) return await this.findByRef(id, ref);
    } catch (err) {
      if (!(err instanceof NotFoundException)) throw err;
    }

    const optionBuilder = new FindOptionsBuilder<Image>();

    optionBuilder
      .setSelect([
        'id',
        'formatType',
        'height',
        'width',
        'url',
        'formatRefName',
      ])
      .extendWhere({ status: ImageStatus.complete })
      .extendWhere({ formatFit: fit })
      .onetimeCondition(type)
      .extendWhere({ formatType: type })
      .onetimeCondition(ref)
      .extendWhere({ formatRefName: ref })
      .extendWhere({ status: ImageStatus.complete })
      .between('width', width, (w: number) => Math.floor(w / 5))
      .between('height', height, (w: number) => Math.floor(w / 5));
    const images = await this.imageRepo.findAllWithId(id, optionBuilder);
    let original: Image | undefined;

    if (images.length === 0) {
      original = await this.imageRepo.findOne(id);
      if (!original) throw new NotFoundException();

      const params = new URLSearchParams({
        ...(options as Record<string, string>), // TODO: search options without cast
        src: await this.storageService.getLink(original.url),
      });
      return {
        url: `/${this.revampUrl}?${params.toString()}`,
        statusCode: 303,
      };
    }

    // TODO: If width and height is not set, it return the smallest - make desition of returning image
    const [image] = Image.sortByAbsoluteFactor(images, {
      height,
      width,
    });

    return {
      url: await this.storageService.getLink(image.url),
      statusCode: 303,
    };
  }

  async removeFormat(id: imageId) {
    const image = await this.imageRepo.findOne(id);
    if (image == null) throw new NotFoundException();
    if (image.formatFit === 'original') {
      throw new BadRequestException(
        'Partial delete on originial is not possible',
      );
    }

    this.imageRepo.remove(image);

    return {
      images: 1,
      metas: 0,
    };
  }

  async remove(id: imageId) {
    // TODO: remove image from storage
    // TODO: remove image from queue to micro service
    // await this.queueService.remove(id);
    const removed = await this.imageRepo.removeAll(id);
    if (removed.images + removed.metas === 0) {
      throw new NotFoundException();
    }
    return removed;
  }
}
