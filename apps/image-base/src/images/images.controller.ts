import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  Query,
  Patch,
  ParseUUIDPipe,
  UploadedFiles,
  BadRequestException,
} from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { Redirect } from '@nestjs/common/decorators/http/redirect.decorator';
import { imageId, MetaBaseWithLinks } from '@api/types/image';
import { PhotoRoles } from '@api/auth/role.decorator';
import { DataResponse } from '@lib/types';
import { RequestQueryOptions } from '@lib/utils/dto/Request.options';
import { ImagesService } from './images.service';

import { ImageOptionsDto } from './dto/image-options.dto';
import { MetaService } from './meta.service';
import { UpdateMetaDto } from './dto/update-meta.dto';
import { FormatOptionsDTO } from './dto/format-options.dto';
import { Image } from './entities/image.entity';
import { Meta } from './entities/meta.entity';

@Controller('images')
export class ImagesController {
  constructor(
    private readonly imagesService: ImagesService,
    private readonly metaService: MetaService,
  ) {}

  /**
   * Upload one or more image to save and analyse them
   *
   * @param files - Multer file
   * @returns {array} ids of uploaded images
   */
  @PhotoRoles()
  @Post()
  @UseInterceptors(AnyFilesInterceptor())
  create(@UploadedFiles() files: Array<Express.Multer.File>) {
    if (!Array.isArray(files)) throw new BadRequestException();
    return Promise.all(files.map((file) => this.imagesService.import(file)));
  }

  /**
   * Request all images of a route
   * @returns - All Images
   */
  @Get()
  findAll(
    @Query() options?: RequestQueryOptions,
  ): DataResponse<MetaBaseWithLinks> {
    return this.metaService.findAll(options?.offset, options?.take);
  }

  /**
   * Get a spezific image, version, resize or manipulate it
   *
   * @param id
   * @param options
   * @returns
   */
  @Get(':id')
  @Redirect()
  findOne(
    @Param('id', ParseUUIDPipe) id: string,
    @Query() options: ImageOptionsDto,
  ) {
    return this.imagesService.findOne(id, options);
  }

  /**
   * delete image by id
   * @param {imageId} id - id of the image
   * @param { boolean } partial - delete only the version of the image
   * @returns
   */
  @PhotoRoles()
  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
    await this.imagesService.remove(id);
  }

  /**
   * delete a specific format version of an image
   * @param {imageId} id - id of the image
   * @param { boolean } partial - delete only the version of the image
   * @returns
   */
  @PhotoRoles()
  @Delete(':id/format')
  async removeFormat(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
    await this.imagesService.removeFormat(id);
  }

  /**
   * Return existing formats of an secific image
   * @param {imageId} id - of the image
   * @param {FormatOptionsDTO} options
   * @returns {DataResponse<PartialImage>}
   */
  @Get(':id/formats')
  findFormats(
    @Param('id', ParseUUIDPipe) id: imageId,
    @Query() options?: FormatOptionsDTO,
  ): Promise<DataResponse<Image>> {
    return this.imagesService.getFormats(id, options);
  }

  /**
   * Retrun metadata to a specific image
   * @param {imageId} id - image id
   * @returns {Promise<Meta>}
   */
  @Get(':id/meta')
  findMeta(@Param('id', ParseUUIDPipe) id: imageId): Promise<Meta> {
    return this.metaService.findOne(id);
  }

  /**
   * Add or remove meta informations
   * @param id
   * @param updateMetaDto
   */
  @PhotoRoles()
  @Patch(':id/meta')
  async patchMeta(
    @Param('id', ParseUUIDPipe) id: imageId,
    @Body() updateMetaDto: UpdateMetaDto,
  ): Promise<void> {
    await this.metaService.patch(id, updateMetaDto);
  }
}
