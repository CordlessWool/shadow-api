export interface ImageQueueInterface {
  uuid: string;
  url: string;
  name: string;
}
