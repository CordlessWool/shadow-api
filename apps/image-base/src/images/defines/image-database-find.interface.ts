export interface ImageDatabaseFindInterface {
  all?: boolean;
  id?: string;
  imageId?: string;
  original?: boolean;
  height?: number | [number, number];
  width?: number | [number, number];
}
