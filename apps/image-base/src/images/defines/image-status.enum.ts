export enum ImageStatus {
  complete = 'complete',
  creating = 'creating',
  error = 'error',
  paused = 'paused',
  deleted = 'deleted',
}
