import { FormatType } from '@lib/enum';

export interface ImageMetaInterface {
  height: number;
  width: number;
  size: number;
  format: FormatType;
}
