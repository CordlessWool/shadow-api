import { Image } from './entities/image.entity';

export const formatDefaultSelectAttributes: (keyof Image)[] = [
  'formatRefName',
  'formatType',
  'width',
  'height',
  'size',
  'status',
];
