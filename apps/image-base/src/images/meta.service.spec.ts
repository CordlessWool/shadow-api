import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { StorageService } from '@api/storage/storage.service';
import { MetaService } from './meta.service';
import { MetaRepository } from './meta.reposotory';

describe('ImageService', () => {
  let service: MetaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MetaService,
        {
          provide: StorageService,
          useValue: {},
        },
        {
          provide: getRepositoryToken(MetaRepository),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<MetaService>(MetaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
