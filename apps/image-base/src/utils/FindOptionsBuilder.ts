import { Between, FindConditions, FindManyOptions } from 'typeorm';
import { attrKeysOf, OnlyAttributes, OverwriteAttributeType } from './types';

type attributes<T> = keyof FindManyOptions<T>;
type whereValue<T> =
  | Partial<OverwriteAttributeType<OnlyAttributes<T>, unknown>>
  | string
  | undefined;
type generalValue<T> = Array<unknown> | whereValue<T> | string;

export enum FindOptionsBuilderExpectionTypes {
  SELECT_RESCTRICTION,
  UNDEFINED_VALUE,
}

// TODO: use some abstract base class or interface for internal errors
export class FindOptionsBuilderExeption extends Error {
  type: FindOptionsBuilderExpectionTypes;

  constructor(type: FindOptionsBuilderExpectionTypes, ...attr: unknown[]) {
    let errMessage;
    switch (type) {
      case FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION:
        errMessage = 'Select condition containes unautoricted keys';
        break;
      case FindOptionsBuilderExpectionTypes.UNDEFINED_VALUE:
        errMessage = `A value is Requiered ${attr[0] ?? ''}`;
        break;
      default:
    }

    super(errMessage);

    this.type = type;
  }
}

export class FindOptionsBuilder<T> implements FindManyOptions<T> {
  select: (keyof T)[];

  where: FindConditions<T> | string;

  #selectRestriction: (keyof T)[] = [];

  #condition = true;

  #useConditionOnce = false;

  #throw = false;

  constructor(base?: Partial<Pick<FindOptionsBuilder<T>, 'select' | 'where'>>) {
    if (base?.select) this.select = base.select;
    if (base?.where) this.where = base.where;
  }

  private isDefined<K>(
    value: K,
    valueName: string | number | symbol | undefined,
  ): value is Exclude<K, undefined> {
    if (this.#throw && value == null) {
      throw new FindOptionsBuilderExeption(
        FindOptionsBuilderExpectionTypes.UNDEFINED_VALUE,
        valueName,
      );
    }

    return value != null;
  }

  private checkConditions(value: generalValue<T> | undefined) {
    const conBuf = this.#condition;
    if (this.#useConditionOnce) {
      this.#condition = true;
    }
    if (!conBuf) return false;
    if (typeof value === 'object' && value != null) {
      if (Array.isArray(value)) {
        return value.length !== 0;
      }
      return Object.keys(value).length !== 0;
    }
    return !!value;
  }

  private throwInCaseOfSelectRestriction(value: (keyof T)[] | undefined) {
    if (!this.#throw || this.checkSelectRestriction(value)) {
      return;
    }

    throw new FindOptionsBuilderExeption(
      FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION,
    );
  }

  private checkSelectRestriction(value: (keyof T)[] | undefined) {
    if (this.#selectRestriction.length === 0) return true;
    if (!value || value.length === 0) return true;
    return value.every((v) => this.#selectRestriction.includes(v));
  }

  private setInCaseOfCondition(att: attributes<T>, value): void {
    if (this.checkConditions(value)) {
      this[att] = value;
    }
  }

  private hasSameType<B>(ref: B, comp: unknown): comp is B {
    return typeof ref === typeof comp;
  }

  private extendInCaseOfCondition(
    att: attributes<T>,
    value: generalValue<T> | undefined,
  ): void {
    if (this[att] == null) {
      this.setInCaseOfCondition(att, value);
      return;
    }
    if (!this.checkConditions(value)) return;
    // TODO: Convert if necassery instead of error message
    if (!this.hasSameType(this[att], value)) {
      throw new Error('attribute and value need to have same type');
    }
    if (typeof value === 'string') {
      this[att] = `${this[att]} ${value.trim()}`;
    } else if (Array.isArray(this[att])) {
      this[att].concat(value);
    } else {
      this[att] = {
        ...this[att],
        ...value,
      };
    }
  }

  setSelectResctriction(value: (keyof T)[], shouldThrow = true): this {
    this.#selectRestriction = value;
    this.shouldThrow(shouldThrow);
    return this;
  }

  removeSelectResctriction(): this {
    this.#selectRestriction = [];
    return this;
  }

  setSelect(value: (keyof T)[] | undefined): this {
    this.throwInCaseOfSelectRestriction(value);
    this.setInCaseOfCondition('select', value);
    return this;
  }

  extendSelect(value: (keyof T)[] | undefined): this {
    this.throwInCaseOfSelectRestriction(value);
    if (this.checkConditions(value)) {
      this.select = [...(this.select || []), ...(value || [])];
    }
    return this;
  }

  setWhere(value: whereValue<T>): this {
    this.setInCaseOfCondition('where', value);
    return this;
  }

  extendWhere<K extends whereValue<T>>(value: K): this {
    this.extendInCaseOfCondition('where', value);
    return this;
  }

  setCondition(value: unknown, once = false): this {
    this.#condition = !!value;
    this.#useConditionOnce = once;
    return this;
  }

  onetimeCondition(value: unknown): this {
    this.setCondition(value, true);
    return this;
  }

  between(
    attr: attrKeysOf<T>,
    value: number | undefined,
    range: ((n: number) => number) | number,
  ): this;
  between(attr: attrKeysOf<T>, value: [number, number] | undefined): this;
  between(
    attr: attrKeysOf<T>,
    value: number | [number, number] | undefined,
    range?: ((n: number) => number) | number,
  ): this {
    if (!this.isDefined(value, attr)) return this;
    let small = 0;
    let large = 0;

    if (Array.isArray(value)) {
      [small, large] = value;
    } else if (typeof range === 'function') {
      small = value - range(value);
      large = value + range(value);
    } else if (range && Number.isFinite(range)) {
      small = value - range / 2;
      large = value + range / 2;
    } else {
      return this;
    }
    const where: whereValue<T> = <whereValue<T>>{
      [attr]: Between(small, large),
    };
    this.extendWhere(where);

    return this;
  }

  // TODO: need to be tested
  shouldThrow(b = true): this {
    this.#throw = b;

    return this;
  }

  get(): FindManyOptions {
    const result: FindManyOptions = {};
    if (this.select) result.select = this.select;
    if (this.where) result.where = this.where;
    return result;
  }
}
