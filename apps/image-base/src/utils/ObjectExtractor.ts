import { RequireExeption } from '@lib/exeptions/RequiredExeption';

export { RequireExeption };

type ObjectExtractorResult<R, O> = { [X in keyof R]: R[X] } & {
  [X in keyof O]?: O[X];
};

export class ObjectExtractor<T extends ObjectExtractorResult<R, O>, R, O> {
  #required: (keyof R)[];

  #attr: (keyof T)[];

  constructor(reqired: (keyof R)[], optional?: (keyof O)[]) {
    this.#attr = Array.from(new Set([...reqired, ...(optional || [])]));
    this.#required = reqired;
  }

  private checkObject(obj: Partial<T>) {
    if (obj && this.#required) {
      const missed: (keyof R)[] = this.#required.filter((attr) => {
        if (obj[<string>attr] == null) {
          return attr;
        }
        return undefined;
      });
      if (missed.length !== 0) {
        throw new RequireExeption(...missed);
      }
    }
  }

  private reduceObject<E extends T>(obj: E): Partial<T> {
    const result: Partial<T> = {};
    return this.#attr.reduce((acc, attr) => {
      if (attr in obj) {
        // eslint-disable-next-line security/detect-object-injection
        acc[attr] = obj[attr];
      }
      return acc;
    }, result);
  }

  reduce<E extends T>(obj: Partial<E>): T {
    this.checkObject(obj);
    return <T>this.reduceObject(obj as E);
  }
}
