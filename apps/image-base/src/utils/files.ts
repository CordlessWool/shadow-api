export const getFileExtension = (path: string): string => {
  const result = /\.(\w+)$/g.exec(path);
  if (result) {
    return result[1];
  }
  throw new Error('path do not match');
};
