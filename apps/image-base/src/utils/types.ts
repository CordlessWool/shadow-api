/* eslint-disable @typescript-eslint/no-explicit-any */
export type typeIfUndefined<T, K> = T extends undefined ? K : never;

export type typeIfFunciton<T, K> = T extends (...params: any) => any
  ? K
  : never;

export type OnlyRequired<T> = {
  [X in keyof T as Exclude<X, typeIfUndefined<T[X], X>>]: T[X];
};

export type OnlyOptional<T> = {
  [X in keyof T as typeIfUndefined<T[X], X>]: T[X];
};

export type OnlyAttributes<T> = {
  [X in keyof T as Exclude<X, typeIfFunciton<T[X], X>>]: T[X];
};

export type OverwriteAttributeType<T, O> = {
  [X in keyof T]: O;
};

export type attributeType<T, P extends keyof T> = T[P];

export type attrKeysOf<T> = {
  [K in keyof T]: T[K] extends (...params: any) => any ? never : K;
}[keyof T];
