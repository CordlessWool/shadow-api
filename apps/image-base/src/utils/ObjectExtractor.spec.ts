import { RequireExeption } from '@lib/exeptions/RequiredExeption';
import * as faker from 'faker';
import { ObjectExtractor } from './ObjectExtractor';

describe('test ObjectExtractor', () => {
  it('reduce object (simple)', async () => {
    const obj = {
      test: faker.lorem.word(),
      someValue: faker.phone.phoneNumber(),
      someOther: faker.lorem.words(),
    };

    const objExtractor = new ObjectExtractor(['test', 'someValue']);
    const reduced = objExtractor.reduce(obj);

    expect(Object.keys(reduced)).toEqual(['test', 'someValue']);
    expect(reduced).toEqual({
      test: obj.test,
      someValue: obj.someValue,
    });
  });

  it('fail on missing value', async () => {
    const obj = {
      test: faker.lorem.word(),
      someValue: faker.phone.phoneNumber(),
      someOther: faker.lorem.words(),
    };

    const objExtractor = new ObjectExtractor([
      'test',
      'someValue',
      'isNotExisting',
    ]);
    const f = () => objExtractor.reduce(obj);
    expect(f).toThrow();
    expect(f).toThrowError(RequireExeption);
  });

  it('reduce object (with not exiting optional)', async () => {
    const obj = {
      test: faker.lorem.word(),
      someValue: faker.phone.phoneNumber(),
      someOther: faker.lorem.words(),
    };

    const objExtractor = new ObjectExtractor(
      ['test', 'someValue'],
      ['optional'],
    );
    const reduced = objExtractor.reduce(obj);

    expect(Object.keys(reduced)).toEqual(['test', 'someValue']);
    expect(reduced).toEqual({
      test: obj.test,
      someValue: obj.someValue,
    });
  });

  it('reduce object (with optional)', async () => {
    const obj = {
      test: faker.lorem.word(),
      someValue: faker.phone.phoneNumber(),
      someOther: faker.lorem.words(),
      optional: faker.random.word(),
    };

    const objExtractor = new ObjectExtractor(
      ['test', 'someValue'],
      ['optional'],
    );
    const reduced = objExtractor.reduce(obj);

    expect(Object.keys(reduced)).toEqual(['test', 'someValue', 'optional']);
    expect(reduced).toEqual({
      test: obj.test,
      someValue: obj.someValue,
      optional: obj.optional,
    });
  });
});
