import { RegExpWrapper } from './RegexpWrapper';
import { UrlExpression } from './UrlExpression';
import { UuidExpression } from './UuidExpression';

export enum Expressions {
  URL = 'URL',
  UUID = 'UUID',
}

type expressionTypes = keyof typeof Expressions;

export class RegExpFactory {
  static expression(
    name: expressionTypes,
    flags?: string,
  ): RegExpWrapper<unknown> {
    switch (name) {
      case 'URL':
        return new UrlExpression(flags);
      case 'UUID':
        return new UuidExpression(flags);
      default:
        throw new Error('Expression name is not defined');
    }
  }
}
