import { UuidExpression } from './UuidExpression';

describe('test UuidExpression', () => {
  it('with new operator', () => {
    const expression = new UuidExpression();
    expression.setValue('48c31adc-5423-45e8-808a-2ff7717c3b46');
    expect(expression.isValid()).toBe(true);
  });

  it('create with static setValue', () => {
    const expression = UuidExpression.setValue(
      '02b6d1df-461b-48f8-9694-53ee39ab0f31',
    );
    expect(expression.isValid()).toBe(true);
  });

  it('with wrong string', () => {
    const expression = new UuidExpression();
    expression.setValue('test affe');
    expect(expression.isValid()).toBe(false);
  });

  it('get()', () => {
    const expression = new UuidExpression();
    expression.setValue('84ead19b-040e-4f9d-bdda-bf3ed103d53a');
    expect(expression.get()).toBe('84ead19b-040e-4f9d-bdda-bf3ed103d53a');
  });
});
