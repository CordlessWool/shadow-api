import { RegExpWrapper } from './RegexpWrapper';

export abstract class ObjectExpression<T> extends RegExpWrapper<T> {
  isValid(): boolean;
  isValid<K extends keyof T>(...required: K[]): K[];
  isValid<K extends keyof T>(...required: K[]): K[] | boolean {
    this.resetIndex();
    const isValid = this.regex.test(this.value);
    if (required.length === 0) return isValid;
    if (isValid) {
      const obj = this.get();
      const keys: (keyof T)[] = Object.keys(<T>obj) as (keyof T)[];
      return required.filter((attr) => !keys.includes(attr));
    }
    return required;
  }
}
