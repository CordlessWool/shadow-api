import { StringExpression } from './StringExpression';

export class UuidExpression extends StringExpression {
  static pattern =
    '^[0-9a-fA-F]{8}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{4}\\b-[0-9a-fA-F]{12}$';

  constructor(flags = 'g') {
    super(UuidExpression.pattern, flags);
  }

  static setValue(str: string): UuidExpression {
    return new UuidExpression().setValue(str);
  }
}
