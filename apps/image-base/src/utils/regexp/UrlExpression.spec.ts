import * as faker from 'faker';
import { RegExpWrapper } from './RegexpWrapper';
import { UrlExpression } from './UrlExpression';

describe('UrlExpression', () => {
  it('create a object and check instanceof', () => {
    const expression = new UrlExpression();
    expect(expression).toBeInstanceOf(UrlExpression);
    expect(expression).toBeInstanceOf(RegExpWrapper);
  });

  describe('isValid()', () => {
    it('URL with query', () => {
      const url =
        'https://regex101.com?test=1234&some=other&somem=more&more=12';
      const expression = new UrlExpression();
      expression.setValue(url);
      expect(expression.isValid()).toBe(true);
    });

    it('URL without schema is not valid', () => {
      const url = 'regex101.com?test=1234&some=other&somem=more&more=12';
      const expression = new UrlExpression();
      expression.setValue(url);
      expect(expression.isValid()).toBe(false);
    });
  });

  describe('get()', () => {
    it('get all', () => {
      const url =
        'https://regex101.com?test=1234&some=other&somem=more&more=12';
      const expected = {
        schema: 'https',
        host: 'regex101.com',
        port: 443,
        queryString: 'test=1234&some=other&somem=more&more=12',
      };
      const expression = new UrlExpression();
      expression.setValue(url);
      expect(expression.get()).toEqual(expected);
    });
    it('get host', () => {
      const url =
        'https://regex101.com?test=1234&some=other&somem=more&more=12';
      const expected = 'regex101.com';
      const expression = new UrlExpression();
      expression.setValue(url);
      expect(expression.get('host')).toBe(expected);
    });
    it('get path', () => {
      const url = 'http://www.test.com/dir/filename.jpg?var1=foo#bar';
      const expected = '/dir/filename.jpg';
      const expression = new UrlExpression();
      expect(expression.setValue(url)).toBeInstanceOf(UrlExpression);
      expect(expression.get('path')).toBe(expected);
    });

    it('validate long url', () => {
      const url =
        'https://aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.de/YUNSW-Baumwolle-Schlafzimmer-Wohnzimmer-K%C3%BCchenvorhang/dp/B083HVB81V/ref=sr_1_3?crid=2ZSROQPOSB39V&keywords=yunsw%2BLeinen&qid=1636839534&qsid=260-9365299-5885064&sprefix=yunsw%2Bleinen%2Caps%2C195&sr=8-3&sres=B08RWMGYLP%2CB083H6KD5D%2CB083HVB81V%2CB083HV5CYN%2CB083H6R3LJ%2CB083H7TL9X%2CB085X62CNZ%2CB085X64XC7%2CB08RW7YDSG%2CB08RW6YXV5%2CB085X613HW%2CB07ZHFGK76%2CB07ZH79YQ4%2CB08RWXY5BY%2CB08RWQ5GL3%2CB083H7X88S%2CB08RW59XXY%2CB08F4SDNNS%2CB08BJMKP3S%2CB077NHKLWW&th=1';
      const expression = new UrlExpression();
      expect(expression.setValue(url)).toBeInstanceOf(UrlExpression);
    });

    it('validate long url with sub url', () => {
      const url =
        'http://www.test.com/dir/filename.jpg?url=https://www.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.de/YUNSW-Baumwolle-Schlafzimmer-Wohnzimmer-K%C3%BCchenvorhang/dp/B083HVB81V/ref=sr_1_3?crid=2ZSROQPOSB39V&keywords=yunsw%2BLeinen&qid=1636839534&qsid=260-9365299-5885064&sprefix=yunsw%2Bleinen%2Caps%2C195&sr=8-3&sres=B08RWMGYLP%2CB083H6KD5D%2CB083HVB81V%2CB083HV5CYN%2CB083H6R3LJ%2CB083H7TL9X%2CB085X62CNZ%2CB085X64XC7%2CB08RW7YDSG%2CB08RW6YXV5%2CB085X613HW%2CB07ZHFGK76%2CB07ZH79YQ4%2CB08RWXY5BY%2CB08RWQ5GL3%2CB083H7X88S%2CB08RW59XXY%2CB08F4SDNNS%2CB08BJMKP3S%2CB077NHKLWW&th=1';
      const expression = new UrlExpression();
      expect(expression.setValue(url)).toBeInstanceOf(UrlExpression);
    });

    it('block request for constructor', () => {
      const url = faker.internet.url();
      const expression = UrlExpression.setValue(url);
      expect(expression).toBeInstanceOf(UrlExpression);
      const t = () => expression.get(<any>'constructor');
      expect(t).toThrowError();
    });

    it('block request for unknown props', () => {
      const url = faker.internet.url();
      const expression = UrlExpression.setValue(url);
      expect(expression).toBeInstanceOf(UrlExpression);
      const t = () => expression.get(<any>'something');
      expect(t).toThrowError();
    });

    it('block request for unknown props', () => {
      const url = 'random://ts.de';
      const expression = UrlExpression.setValue(url);
      expect(expression).toBeInstanceOf(UrlExpression);
      expect(expression.get('port')).toBeUndefined();
    });
  });
});
