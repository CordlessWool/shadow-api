import { BadRequestException } from '@nestjs/common';
import { ObjectExpression } from './ObjectExpression';

interface UrlResults {
  schema: string;
  host: string;
  port?: number;
  path?: string;
  queryString?: string;
}

type getAll = UrlResults | undefined;
type getPart<K extends keyof UrlResults> = UrlResults[K];
type getRes<K extends keyof UrlResults> = getAll | getPart<K>;

export class UrlExpression extends ObjectExpression<UrlResults> {
  // (?:([w]*)://) - Match Schema, but return only schema name without ://
  // ([w-.]+) - host
  // (?:[:]([\d]+){1})? - port
  // (/[w-./]+)? - path
  // (?:(?:?([w]+=[w]+){1})?(?:&([w]+=[w]+){1})*)? - query elements
  // ([#]?[w]+)? - ref
  // (?:[:](\d)+)?
  static pattern =
    '^(?:([\\w]*):\\/\\/)([\\w\\-\\.]+)(?:[:]([\\d]+){1})?\\/?(\\/[\\w\\-\\.\\/]+)?(?:[?]([&%\\-\\w=]*))?([#]?[\\w]+)?$';

  constructor(flags = 'g') {
    super(UrlExpression.pattern, flags);
  }

  static setValue(str: string): UrlExpression {
    return new UrlExpression().setValue(str);
  }

  private mapSchemaToPort(schema: string) {
    switch (schema) {
      case 'http':
        return 80;
      case 'https':
        return 443;
    }

    return undefined;
  }

  private setResult(match: RegExpExecArray | null) {
    if (match) {
      this.result = {
        schema: match[1],
        host: match[2],
        port: Number.parseInt(match[3], 10) || this.mapSchemaToPort(match[1]),
        path: match[4],
        queryString: match[5],
      };
    }
  }

  private allOrOne<K extends keyof UrlResults>(attr?: K): getRes<K> {
    if (attr && typeof this.result === 'object') {
      if (this.result.hasOwnProperty(attr)) {
        // eslint-disable-next-line security/detect-object-injection
        return this.result[attr];
      }

      throw new BadRequestException(`${attr} does not exist`);
    }
    return this.result;
  }

  get(): getAll;
  get<K extends keyof UrlResults>(attr: K): getPart<K>;
  get<K extends keyof UrlResults>(attr?: K): getRes<K> {
    // TODO: make this prove for not posible matches
    this.resetIndex();
    this.throwErrorIfValueIsNotDefined();
    if (!this.result) {
      const match = this.regex.exec(this.value);
      this.setResult(match);
    }

    return this.allOrOne(attr);
  }
}
