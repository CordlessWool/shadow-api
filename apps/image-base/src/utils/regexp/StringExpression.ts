import { RegExpWrapper } from './RegexpWrapper';

export abstract class StringExpression extends RegExpWrapper<string> {
  get(): string {
    this.throwErrorIfValueIsNotDefined();
    this.regex.lastIndex = 0;
    const match = this.regex.exec(this.value);
    if (!match) return '';
    return match[0];
  }
}
