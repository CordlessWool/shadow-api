import { InternalServerErrorException } from '@nestjs/common';

export abstract class RegExpWrapper<T> {
  protected regex: RegExp;

  protected value: string;

  protected result: T | undefined;

  constructor(pattern: string, flags?: string) {
    this.regex = new RegExp(pattern, flags);
  }

  protected throwErrorIfValueIsNotDefined(errorMessage?: string) {
    if (!this.value) throw new InternalServerErrorException(errorMessage);
  }

  setValue(str: string) {
    this.value = str;
    this.result = undefined;
    this.resetIndex();
    return this;
  }

  getRegexp(): RegExp {
    return this.regex;
  }

  resetIndex() {
    this.regex.lastIndex = 0;
  }

  next(): string | null {
    const match = this.regex.exec(this.value);
    if (match != null) return match[1];
    return null;
  }

  isValid() {
    this.resetIndex();
    return this.regex.test(this.value);
  }

  abstract get(): T | undefined;

  abstract get<K extends keyof T>(attr: keyof K): T[K] | undefined;

  abstract get<K extends keyof T>(attr?: keyof K): T[K] | T | undefined;
}
