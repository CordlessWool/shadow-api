import { Tag } from '@api/tags/entities/tag.entity';
import { Image } from '@api/images/entities/image.entity';
import * as faker from 'faker';
import { Between } from 'typeorm';
import {
  FindOptionsBuilder,
  FindOptionsBuilderExeption,
  FindOptionsBuilderExpectionTypes,
} from './FindOptionsBuilder';

describe('test FindOptionBuilder', () => {
  it('ignore undefined value for where', () => {
    const findManyOptions = new FindOptionsBuilder<Tag>({
      select: ['id'],
    });
    findManyOptions.setWhere({});

    expect(findManyOptions.get()).toEqual({
      select: ['id'],
    });
  });

  it('set where condition', () => {
    const findManyOptions = new FindOptionsBuilder<Tag>({
      select: ['id'],
    });
    findManyOptions.setWhere({
      id: 'test',
    });

    expect(findManyOptions.get()).toEqual({
      select: ['id'],
      where: {
        id: 'test',
      },
    });
  });

  it('allow extend without calling set', () => {
    const optionsBuilder = new FindOptionsBuilder<Image>();
    const now = new Date();
    optionsBuilder
      .extendWhere({
        createdAt: now,
      })
      .extendSelect(['id', 'createdAt']);

    expect(optionsBuilder.get()).toEqual({
      select: ['id', 'createdAt'],
      where: {
        createdAt: now,
      },
    });
  });

  it('call with undefined', () => {
    const keepThis: any = {
      select: ['id'],
      where: {
        id: faker.datatype.uuid,
      },
    };
    const optionsBuilder = new FindOptionsBuilder<Image>(keepThis);

    optionsBuilder
      .setSelect(undefined)
      .extendWhere(undefined)
      .setWhere(undefined)
      .extendSelect(undefined)
      .setSelect(undefined);

    expect(optionsBuilder.get()).toEqual(keepThis);
  });

  describe('test select restriction', () => {
    it('set select restriction', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      optionBuilder
        .setSelectResctriction(['id', 'createdAt'])
        .setSelect(['id'])
        .extendSelect(['createdAt']);

      expect(optionBuilder.get().select).toEqual(['id', 'createdAt']);
    });

    it('call select with undefined', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      optionBuilder
        .setSelectResctriction(['id', 'createdAt'])
        .setSelect(undefined)
        .extendSelect(undefined);

      expect(optionBuilder.get()).toEqual({});
    });

    it('restrict select against unwanted values (setSelect)', () => {
      try {
        const optionBuilder = new FindOptionsBuilder<Tag>();
        optionBuilder
          .setSelectResctriction(['id', 'createdAt'])
          .setSelect(['description']);
        expect('Should not reach this point').toBeUndefined();
      } catch (err) {
        expect(err).toBeInstanceOf(FindOptionsBuilderExeption);
        expect(err.type).toBe(
          FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION,
        );
      }
    });

    it('restrict select against unwanted values (extendSelect)', () => {
      try {
        const optionBuilder = new FindOptionsBuilder<Image>();
        optionBuilder
          .setSelectResctriction(['id', 'createdAt'])
          .setSelect(['createdAt'])
          .extendSelect(['height']);
        expect('Should not reach this point').toBeUndefined();
      } catch (err) {
        expect(err).toBeInstanceOf(FindOptionsBuilderExeption);
        expect(err.type).toBe(
          FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION,
        );
      }
    });

    it('reset resctriction', () => {
      const optionBuilder = new FindOptionsBuilder<Tag>();
      optionBuilder.setSelectResctriction(['id', 'createdAt']);
      try {
        optionBuilder.setSelect(['description']);
        expect('Should not reach this point').toBeUndefined();
      } catch (err) {
        expect(err).toBeInstanceOf(FindOptionsBuilderExeption);
        expect(err.type).toBe(
          FindOptionsBuilderExpectionTypes.SELECT_RESCTRICTION,
        );
      }

      optionBuilder.removeSelectResctriction().extendSelect(['description']);

      expect(optionBuilder.get().select).toEqual(['description']);
    });
  });

  describe('test between condition', () => {
    it('should return this', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      const res = optionBuilder.between('height', [300, 1000]);
      expect(res).toBeInstanceOf(FindOptionsBuilder);
    });

    it('set where condition', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      const res = optionBuilder.between('height', [300, 700]);
      expect(res.get()).toEqual({
        where: {
          height: Between(300, 700),
        },
      });
    });

    it('set a fix range', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      const res = optionBuilder.between('width', 700, 100);
      expect(res.get()).toEqual({
        where: {
          width: Between(650, 750),
        },
      });
    });

    it('set a variable range', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      const calc = (n: number) => n * 0.5;
      const res = optionBuilder.between('width', 500, calc);
      expect(res.get()).toEqual({
        where: {
          width: Between(250, 750),
        },
      });
    });

    it('cobined with other functions', () => {
      const optionBuilder = new FindOptionsBuilder<Image>();
      const calc = (n: number) => n * 0.5;
      const res = optionBuilder
        .setWhere({ id: 'some uuid' })
        .between('width', 500, calc)
        .extendWhere({
          height: 30,
        })
        .setSelect(['id']);
      expect(res.get()).toEqual({
        select: ['id'],
        where: {
          id: 'some uuid',
          width: Between(250, 750),
          height: 30,
        },
      });
    });
  });
});
