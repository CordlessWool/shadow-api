FROM node:16-alpine

WORKDIR /app

EXPOSE 3001

ENV SHADOW_CONFIG_LOCATION=/app/config.yml

RUN npm install -g npm

COPY . .


CMD 'npm start image-base'