import { ValidationError } from 'class-validator';

export class ValidationExeption extends Error {
  static fromClassValidator(errors: ValidationError[]): ValidationExeption {
    return new ValidationExeption(errors.toString());
  }
}
