import { InternalServerErrorException } from '@nestjs/common';

export class RequireExeption extends InternalServerErrorException {
  #missed: Array<number | string | symbol>;

  constructor(...requieredValues: Array<number | string | symbol>) {
    super(`Missed required Values: ${requieredValues}.join(' ')`);
    this.#missed = requieredValues;
  }

  getRequieredValues() {
    return this.#missed;
  }
}
