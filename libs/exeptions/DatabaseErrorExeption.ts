import { InternalServerErrorException } from '@nestjs/common';

export enum DatabaseErrorTypes {
  DUPLICATE,
  UNKNOWN,
}

type DatabaseErrorExeptionSupportedDbType = 'postgres' | 'mysql' | 'sqlite';

export class DatabaseErrorExeption extends Error {
  readonly type: DatabaseErrorTypes;

  readonly original: Record<string, unknown>;

  constructor(
    type: DatabaseErrorTypes,
    message: string,
    err?: Record<string, unknown>,
  ) {
    super(message);
    this.type = type;
    if (err) this.original = err;
  }

  protected static postgresByCodeToType(code: number) {
    switch (code) {
      case 23505:
        return DatabaseErrorTypes.DUPLICATE;
    }
    return undefined;
  }

  protected static mysqlByCodeToType(code: number) {
    switch (code) {
      case 1062:
        return DatabaseErrorTypes.DUPLICATE;
    }
    return undefined;
  }

  protected static sqliteByCodeToType(code: number) {
    switch (code) {
      case 2067:
        return DatabaseErrorTypes.DUPLICATE;
    }
    return undefined;
  }

  protected static getDatabaseFunktion(
    database: DatabaseErrorExeptionSupportedDbType,
  ): CallableFunction {
    if (database === 'postgres') {
      return DatabaseErrorExeption.postgresByCodeToType;
    }
    if (database === 'mysql') {
      return DatabaseErrorExeption.mysqlByCodeToType;
    }
    if (database === 'sqlite') {
      return DatabaseErrorExeption.sqliteByCodeToType;
    }

    throw new InternalServerErrorException('Database is not Supported');
  }

  static createByCode(
    database: DatabaseErrorExeptionSupportedDbType,
    code: number,
    err: Record<string, unknown>,
  ): DatabaseErrorExeption {
    const codeToTypeFu = DatabaseErrorExeption.getDatabaseFunktion(database);
    const type = codeToTypeFu(code) || DatabaseErrorTypes.UNKNOWN;
    const message = <string>(err.detail || err.message);
    return new DatabaseErrorExeption(type, message, err);
  }
}
