export class AttributeValueExeption<T, K extends keyof T> extends Error {
  protected obj: T;

  protected attr: K;

  constructor(obj: T, attr: K) {
    super(`${typeof obj} is missing a value for ${attr}`);
    this.obj = obj;
    this.attr = attr;
  }

  getObject(): T {
    return this.obj;
  }

  getAttributeName(): K {
    return this.attr;
  }

  isUndefined(): boolean {
    return this.getAttributeValue() == null;
  }

  getAttributeValue(): T[K] {
    return this.obj[this.attr];
  }
}
