import { InternalServerErrorException } from '@nestjs/common';

export class StorageErrorExeption extends InternalServerErrorException {}
