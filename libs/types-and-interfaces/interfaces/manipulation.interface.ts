import { FormatFit, FormatType } from '../enum';

export interface ManipulationInterface {
  height?: number;
  width?: number;
  fit?: FormatFit;
  type?: FormatType;
}
