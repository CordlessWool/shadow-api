import { FormatType } from '../enum';

export interface StorageOptionsInterface {
  mimeType?: string;
  size?: number;
}

export interface StorageInterface {
  uuid: string;

  name: string;

  file?: Buffer;

  format: FormatType;

  isPrivate?: boolean;

  options?: StorageOptionsInterface;
}

export class ExtendedStorageInterface implements StorageInterface {
  folder: string;

  uuid: string;

  name: string;

  file?: Buffer;

  format: FormatType;

  isPrivate?: boolean;

  options?: StorageOptionsInterface;
}
