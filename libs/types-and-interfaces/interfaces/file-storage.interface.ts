import {
  ExtendedStorageInterface,
  StorageInterface,
} from './storage.interface';
import { SelectedRequired } from '../util.types';

export interface FileStorageServiceInterface {
  generateUrl(data: ExtendedStorageInterface): string;
  getLink(url: string): Promise<string>;
  getUploadLink(data: string | StorageInterface): Promise<string>;
  get(url: string): Promise<Buffer>;
  save(
    data: SelectedRequired<ExtendedStorageInterface, 'file'>,
  ): Promise<string>;
  updatePermissions(): Promise<void>;
  remove(urls: string[]): Promise<void>;
}
