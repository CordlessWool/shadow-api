export interface DataServiceInterface<T> {
  save(value: T): Promise<unknown>;
  find?(options?: Record<string, unknown>): Promise<unknown>;
  findOne?(
    id: number | string,
    options?: Record<string, unknown>,
  ): Promise<unknown>;
}
