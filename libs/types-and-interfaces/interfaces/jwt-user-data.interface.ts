import { Role } from '@lib/enum';

export interface JwtUserData {
  userId: string;
  name: string;
  email: string;
  role: Role;
  accountId: string;
}
