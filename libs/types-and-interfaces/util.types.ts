export type PartialRequired<T, R extends keyof T, P extends keyof T> = Omit<
  T,
  R | P
> &
  Required<Pick<T, R>> &
  Partial<Pick<T, P>>;

export type SelectedRequired<T, R extends keyof T> = Omit<T, R> &
  Required<Pick<T, R>>;

export type SelectedPartial<T, P extends keyof T> = Omit<T, P> &
  Partial<Pick<T, P>>;
