export * from './interfaces/file-storage.interface';
export * from './interfaces/storage.interface';
export * from './interfaces/manipulation.interface';
export * from './interfaces/DataService.interface';
export * from './interfaces/jwt-user-data.interface';
