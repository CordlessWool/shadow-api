export enum Role {
  owner = 'owner',
  admin = 'admin',
  editor = 'editor',
  photographer = 'photographer',
  author = 'author',
  public = 'non-auth',
}
