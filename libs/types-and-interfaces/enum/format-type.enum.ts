export enum FormatType {
  jpg = 'jpg',
  png = 'png',
  webp = 'webp',
  gif = 'gif',
}
