export type DataResponse<T> = Promise<{
  total: number;
  offset?: number;
  taken?: number;
  data: T[];
}>;
