import { ValidationExeption } from '@lib/exeptions/validation.exeption';
import { ClassTransformOptions } from '@nestjs/common/interfaces/external/class-transform-options.interface';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { validateSync, ValidatorOptions } from 'class-validator';

export function transformAndValidate<T extends ClassConstructor<unknown>>(
  baseClass: T,
);
export function transformAndValidate<T extends ClassConstructor<unknown>>(
  baseClass: T,
  transformOptions: ClassTransformOptions,
);
export function transformAndValidate<T extends ClassConstructor<unknown>>(
  baseClass: T,
  transformOptions: ClassTransformOptions,
  validatorOptions: ValidatorOptions,
);
export function transformAndValidate<T extends ClassConstructor<unknown>>(
  baseClass: T,
  transformOptions?: ClassTransformOptions,
  validatorOptions: ValidatorOptions = {
    skipMissingProperties: false,
  },
) {
  return (config: Record<string, unknown>) => {
    const validatedConfig = <T>(
      plainToClass(baseClass, config, transformOptions)
    );

    const errors = validateSync(validatedConfig, validatorOptions);

    if (errors.length > 0) {
      throw new ValidationExeption(errors.toString());
    }
    return validatedConfig;
  };
}
