import {
  registerDecorator,
  ValidationOptions,
  isHexColor,
} from 'class-validator';

export const IsHexRgb =
  (validationOptions?: ValidationOptions) =>
  // eslint-disable-next-line @typescript-eslint/ban-types
  (object: Object, propertyName: string) => {
    registerDecorator({
      name: 'isHexRgb',
      target: object.constructor,
      propertyName,
      options: validationOptions,
      validator: {
        validate(value: unknown) {
          if (
            typeof value === 'string' &&
            (value.length === 4 || value.length === 7)
          ) {
            return value.startsWith('#') && isHexColor(value);
          }
          return false;
        },
      },
    });
  };
