import 'reflect-metadata';
import { Type } from 'class-transformer';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { ValidationExeption } from '@lib/exeptions/validation.exeption';
import { transformAndValidate } from './transformAndValidate';

class Test {
  @IsBoolean()
  transform: boolean;

  @IsString()
  text: string;

  @Type(() => Number)
  @IsNumber()
  number: number;

  @IsOptional()
  @IsString()
  optional: string;
}

describe('test transformAndValidate', () => {
  it('valid file', () => {
    const base = {
      transform: false,
      text: 'someText',
      number: 0,
    };

    const testObject = transformAndValidate(Test)(base);
    expect(testObject).toBeInstanceOf(Test);
    expect(testObject.transform).toBe(base.transform);
    expect(testObject.number).toBe(base.number);
    expect(testObject.text).toBe(base.text);
    expect(testObject.optional).toBeUndefined();
  });

  it('transform type', () => {
    const base = {
      transform: false,
      text: 'someText',
      number: '0800',
    };
    const testObject = transformAndValidate(Test)(base);
    expect(testObject).toBeInstanceOf(Test);
    expect(testObject.transform).toBe(base.transform);
    expect(testObject.number).toBe(Number(base.number));
    expect(testObject.text).toBe(base.text);
    expect(testObject.optional).toBeUndefined();
  });

  it('fail transformation', () => {
    const base = {
      transform: false,
      text: 'someText',
      number: 'test',
      optional: { tst: 't' },
    };
    const t = () => transformAndValidate(Test)(base);
    expect(t).toThrow(ValidationExeption);
  });

  it('invalid file', () => {
    const base = {
      transform: false,
      text: 'someText',
      number: 0,
      optional: { tst: 't' },
    };
    const t = () => transformAndValidate(Test)(base);
    expect(t).toThrow(ValidationExeption);
  });
});
