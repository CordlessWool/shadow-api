import { validateSync } from 'class-validator';
import { IsHexRgb } from './IsHexRgb';

class Colors {
  @IsHexRgb()
  color: string;
}

describe('Test IsHexRgb', () => {
  let c: Colors;

  beforeEach(() => {
    c = new Colors();
  });

  it('3 digets', async () => {
    c.color = '#3AF';
    expect(validateSync(c)).toEqual([]);
  });

  it('6 digets', async () => {
    c.color = '#3AF77E';
    expect(validateSync(c)).toEqual([]);
  });

  it('fail on 7 digits', () => {
    c.color = '#EEEEEE7';
    expect(validateSync(c)).not.toEqual([]);
  });

  it('fail on 4 digits', () => {
    c.color = '#EEE4';
    expect(validateSync(c)).not.toEqual([]);
  });

  it('requie starting hash', () => {
    c.color = '333';
    expect(validateSync(c)).not.toEqual([]);
  });

  it('only hex', async () => {
    c.color = '#3GF';
    expect(validateSync(c)).not.toEqual([]);
  });
});
