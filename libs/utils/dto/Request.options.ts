import { IsNumber, IsOptional } from 'class-validator';

export class RequestQueryOptions {
  @IsOptional()
  @IsNumber()
  offset?: number;

  @IsOptional()
  @IsNumber()
  take?: number;
}
