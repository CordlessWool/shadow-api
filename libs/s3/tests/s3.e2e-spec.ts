import { Test, TestingModule } from '@nestjs/testing';
import { Client } from 'minio';
import { PROVIDER_INDENITIFIER } from '../src/statics';
import { S3Module } from '../src/s3.module';
import * as config from './config.json';
import * as gitlabConfig from './gitlab.config.json';

describe('test S3 minio lib', () => {
  let client: Client;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        S3Module.forRootAsync({
          useFactory: () => (process.env.GITLAB ? gitlabConfig : config),
        }),
      ],
    }).compile();

    client = module.get<Client>(PROVIDER_INDENITIFIER);
  });

  it('create and delete bucket', async () => {
    try {
      const promise = client.makeBucket('test', 'eu-central-1');
      expect(promise).toBeInstanceOf(Promise);
      await promise;

      const exist = await client.bucketExists('test');

      expect(exist).toBe(true);
    } catch (e) {
      console.error(e);
      expect('should not throw an error').toBeUndefined();
    } finally {
      await client.removeBucket('test');
      const stillExist = await client.bucketExists('test');
      expect(stillExist).toBe(false);
    }
  });
});
