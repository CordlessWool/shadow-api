import { FactoryProvider, ModuleMetadata } from '@nestjs/common';
import { ClientOptions } from 'minio';

export interface AsyncConfiguration extends Pick<ModuleMetadata, 'imports'> {
  useFactory: (...args) => ClientOptions;
  inject?: FactoryProvider['inject'];
  global?: boolean;
  name?: string;
}
