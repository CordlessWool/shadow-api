import { Inject } from '@nestjs/common';
import { PROVIDER_INDENITIFIER } from './statics';

export const InjectMinioS3 = (name: string | symbol = PROVIDER_INDENITIFIER) =>
  Inject(name);
