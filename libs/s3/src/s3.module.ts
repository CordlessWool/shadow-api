import { DynamicModule, Module, Provider } from '@nestjs/common';
import { Client, ClientOptions } from 'minio';
import { AsyncConfiguration } from './types';
import { CONFIG_OPTIONS, PROVIDER_INDENITIFIER } from './statics';

@Module({})
export class S3Module {
  static forRoot(
    options: { global?: boolean; name?: string } & ClientOptions,
  ): DynamicModule {
    return S3Module.createModule(
      {
        provide: CONFIG_OPTIONS,
        useValue: options,
      },
      [],
      options.global,
      options.name,
    );
  }

  static forRootAsync(options: AsyncConfiguration): DynamicModule {
    return S3Module.createModule(
      {
        provide: CONFIG_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      },
      options.imports,
      options.global,
      options.name,
    );
  }

  private static createModule(
    optionsProvider: Provider,
    imports: AsyncConfiguration['imports'] = [],
    global = false,
    name: string | symbol = PROVIDER_INDENITIFIER,
  ): DynamicModule {
    return {
      global,
      module: S3Module,
      imports,
      providers: [
        optionsProvider,
        {
          provide: name,
          useFactory: (config: ClientOptions) => {
            return new Client(config);
          },
          inject: [CONFIG_OPTIONS],
        },
      ],
      exports: [PROVIDER_INDENITIFIER],
    };
  }
}
